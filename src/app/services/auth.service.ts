import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Values } from "../values/values";

@Injectable()
export class AuthService {

    constructor(private http: HttpClient) { }

    isAuthenticated(): Promise<boolean> {

        var headers = {
            'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ''),
            'Content-Type': 'application/json'
        }

        return new Promise((resolve, reject) => {
            if (Values.readString(Values.USER, "") != "" && Values.readString(Values.USER, "") != undefined) {
                var user = JSON.parse(Values.readString(Values.USER, ""));
                var userId = user._id;

                if (userId != "" && userId != undefined && userId != null) {
                    this.http.get(Values.BASE_URL + `users/${userId}`, { headers: headers }).subscribe((res: any) => {
                        Values.writeString(Values.USER, JSON.stringify(res.data));
                        resolve(true)
                    }, (error) => {
                        Values.writeString(Values.USER, "");
                        reject(false);
                    })
                } else {
                    reject(false);
                }
            } else {
                reject(false);
            }
        })

    }

}