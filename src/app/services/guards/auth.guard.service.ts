import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { AuthService } from "../auth.service";
import { RouterExtensions } from "nativescript-angular/router";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private routerExtensions: RouterExtensions) { }

    canActivate(): Promise<boolean> {

        return new Promise((resolve, reject) => {
            this.authService.isAuthenticated().then((res) => {
                return resolve(true);
            }, error => {
                this.routerExtensions.navigate(['/login'], { clearHistory: true })
                return reject(false);
            })

        })
    }

}