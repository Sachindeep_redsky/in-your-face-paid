
import { Values } from '~/app/values/values';
var en = require("./en")
var lithuanian = require("./lithuanian")
import { Injectable } from "@angular/core";
import { Subject } from "rxjs";


@Injectable()
export class LanguageService {
    private _languageState = new Subject<any>();

    languageState = this._languageState.asObservable();
    public langauge: any;
    public langaugeList = [
        { value: "en", display: "English" },
        { value: "lithuanian", display: "Lithuanian" }
    ]

    constructor() { }

    setLanguage(language: string) {

        // if (!(Values.doesExist(Values.LANGUAGE))) {
        //     Values.writeString(Values.LANGUAGE, language);


        // }

        var reqVar;
        switch (language) {
            case 'en':
                reqVar = en.lang();
                console.log("english selected");
                Values.writeString(Values.LANGUAGE, language);
                break;
            case 'lithuanian':
                reqVar = lithuanian.lang()
                console.log("lithuanian selected");
                Values.writeString(Values.LANGUAGE, language);
                break;

            default:
                reqVar = en.lang();
                console.log("default selected");
                Values.writeString(Values.LANGUAGE, language);
                break;
        }
        // return reqVar;
        this.langauge = reqVar;
        this._languageState.next(reqVar);

    }

}





// function lang() {

//     var reqVar;
//     switch (appSetting.getString('language', 'en')) {
//         case 'en':
//             reqVar = en.lang();
//             console.log("english selected");
//             break;
//         case 'lithuanian':
//             reqVar = lithuanian.lang()
//             console.log("lithuanian selected");
//             break;

//         default:
//             reqVar = en.lang();
//             console.log("default selected");
//             break;
//     }
//     return reqVar;
// }

// exports.lang = lang;
