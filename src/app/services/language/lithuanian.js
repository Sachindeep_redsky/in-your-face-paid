var obj = {
    goodMorning: "Labas rytas",
    paymentPaid: "Mokama",
    settings: "Nustatymai",
    setquizreminder: "Nustatyti viktorinos priminimą",
    changePassword: "Pakeisti slaptažodį",
    logout: "Atsijungti",
    quizButton: "Viktorina",
    nextChapterButton: "Kitas skyrius",
    likeDialogText: "patinka tai, ką skaitai?",
    buyButton: "Pirkite visišką prieigą",
    continueButton: "Tęsti",
    hello: "Sveiki",
    activityText: "Veikla",
    quizDetails: "Viktorinos informacija",
    eiusmodDo: "Eiusmod padaryti",
    typeBioHere: "Čia įveskite bio",
    editButton: "REDAGUOTI",
    chapterText: "Skyrius",
    updateText: "Atnaujinti",
    discardText: "Išmeskite",
    bookmarkHeading: "Jūsų žymės",
    changePasswordHeading: "Pakeisti slaptažodį",
    oldPasswordHint: "Senas slaptažodis",
    newPasswordHint: "Naujas Slaptažodis",
    confirmPasswordhint: "Patvirtinti slaptažodį",
    updatePasswordButton: "Atnaujinkite slaptažodį",
    confirmOtpHeading: "Įveskite savo OTP kodą",
    otpHint: "Įveskite savo OTP",
    confirmButton: "Patvirtinti",
    emailHint: "Įveskite el. Pašto adresą",
    sendOTPButton: "Siųsti OTP",
    forgotHeading: "Pamiršote slaptažodį",
    loginHeading: "Prisijungti",
    usernameHint: "Pašto adresą",
    passwordHint: "Slaptažodis",
    dontAccount: "Neturite sąskaitos?",
    createNow: "Sukurti dabar",
    loginButton: "Prisijungti",
    registerHeading: "Registruotis",
    nameHint: "Įveskite vardą",
    passwordHint: "Įvesti slaptažodį",
    onlyemailHint: "Pašto adresą",
    alreadyAccount: "Jau turite paskyrą?",
    tryAgain: "Prašau, pabandykite dar kartą",
    somethingWentWrong: "Kažkas negerai",
    changeLanguage: "Pakeisti KALBĄ",
    question: "Klausimas",
    search: "Paieška",
    topics: "Temos",
    bookmark: "Skirtukas",
    account: "Sąskaita",
    selectDay: "Pasirinkite dieną",
    selectTime: "Pasirinkite laiką",
    done: "padaryta",
    cancel: "Atšaukti"

}

function lang() {
    console.log('obj:::', obj)
    return obj;
}

exports.lang = lang;
