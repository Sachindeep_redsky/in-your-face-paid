import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable()
export class PlayerService {
    private _playerState = new Subject<string>();

    playerState = this._playerState.asObservable();

    constructor() { }

    toggle(chapterName: string) {
        this._playerState.next(chapterName);
    }

}