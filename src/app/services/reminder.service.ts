import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable()
export class ReminderService {
    private _reminderState = new Subject<boolean>();

    reminderState = this._reminderState.asObservable();

    constructor() { }

    showReminder() {
        this._reminderState.next(true);
    }

}