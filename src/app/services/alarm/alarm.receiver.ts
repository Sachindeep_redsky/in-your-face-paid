/// <reference path="../../../../node_modules/tns-platform-declarations/ios.d.ts" />
/// <reference path="../../../../node_modules/tns-platform-declarations/android/android-platform-28.d.ts" />

import { Values } from "~/app/values/values";

var utils = require("utils/utils");

declare module com {
    export module tns {
        export class NativeScriptActivity extends java.lang.Object {
            public static class: java.lang.Class<com.tns.NativeScriptActivity>;
        }
        export class NotificationAlarmReceiver extends java.lang.Object {
            public static class: java.lang.Class<com.tns.NotificationAlarmReceiver>;
        }
    }
}

@JavaProxy('com.tns.NotificationAlarmReceiver')

class NotificationAlarmReceiver extends android.content.BroadcastReceiver {

    private static TAG: string = "NotificationAlarmRcvr";

    constructor() {
        super();
    }

    public onReceive(context: android.content.Context, intent: android.content.Intent) {
        var action = intent.getAction();
        if ("ACTION_START_NOTIFICATION_SERVICE" == action) {
            this.postNotification();
        }
    }

    postNotification() {
        var context = utils.ad.getApplicationContext();// get a reference to the application context in Android
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            var name = "InYourFace";
            var description = "Reminds about inyourface schedules";
            var importance = android.app.NotificationManager.IMPORTANCE_DEFAULT;
            var channel = new android.app.NotificationChannel("555", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            var notificationManager = context.getSystemService(android.app.NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
            var builder = new android.app.Notification.Builder(context, "555");
        } else {
            var builder = new android.app.Notification.Builder(context);
        }

        builder.setContentTitle("In Your Face");
        builder.setAutoCancel(true);
        builder.setContentText("It is time to read something here");
        builder.setVibrate([100, 200, 100])// optional
        builder.setSmallIcon(utils.ad.resources.getDrawableId("view_white"));
        // will open main NativeScript activity when the notification is pressed
        var mainIntent = new android.content.Intent(context, com.tns.NativeScriptActivity.class);
        var pendingIntent = android.app.PendingIntent.getActivity(context,
            1,
            mainIntent,
            android.app.PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        var manager = context.getSystemService(android.content.Context.NOTIFICATION_SERVICE);

        var selectedDays = JSON.parse(Values.readString(Values.REMINDER, "[]"))
        if (selectedDays != undefined && selectedDays[new Date().getDay()] != undefined)
            if (selectedDays[new Date().getDay()] == true) {
                manager.notify(1, builder.build());
            }
    }
}