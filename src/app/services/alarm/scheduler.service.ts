import { Values } from '~/app/values/values';
import { AlarmUpdater } from "./alarm.updater";

/// <reference path="../../../../node_modules/tns-platform-declarations/ios.d.ts" />
/// <reference path="../../../../node_modules/tns-platform-declarations/android.d.ts" />

declare module com {
    export module tns {
        export class NativeScriptActivity extends java.lang.Object {
            public static class: java.lang.Class<com.tns.NativeScriptActivity>;
        }
        export class NotificationAlarmReceiver extends java.lang.Object {
            public static class: java.lang.Class<com.tns.NotificationAlarmReceiver>;
        }
    }
}


@JavaProxy('com.tns.NotificationRestoreAlarmReceiver')

class NotificationRestoreReceiver extends android.content.BroadcastReceiver {

    private static TAG: string = "NotifyRestoreReceiver";

    constructor(private alarmUpdater: AlarmUpdater) {
        super();
    }

    public onReceive(context: android.content.Context, intent: android.content.Intent): void {
        if (context == null || !(android.content.Intent.ACTION_BOOT_COMPLETED == intent.getAction())) {
            return;
        }

        this.scheduleNotification(context);
    }

    public scheduleNotification(context: android.content.Context) {

        var triggerTime = Values.readNumber(Values.ALARM_TIME, new Date().getTime());

        var alarmManager = <android.app.AlarmManager>context.getSystemService(android.content.Context.ALARM_SERVICE);

        try {
            var notificationIntent = new android.content.Intent(context, com.tns.NotificationAlarmReceiver.class)
            notificationIntent.setAction("ACTION_START_NOTIFICATION_SERVICE");

            var pendingIntent = android.app.PendingIntent.getBroadcast(context, 0, notificationIntent, android.app.PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager.cancel(pendingIntent);
            alarmManager.setRepeating(android.app.AlarmManager.RTC_WAKEUP, triggerTime, android.app.AlarmManager.INTERVAL_DAY, pendingIntent);

        } catch (e) {
            console.log('reminderService Error', e)
        }
    }

}