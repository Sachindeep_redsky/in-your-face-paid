import { isAndroid } from 'platform';
import { Injectable } from "@angular/core";

var utils = require("utils/utils");


declare module com {
    export module tns {
        export class NativeScriptActivity extends java.lang.Object {
            public static class: java.lang.Class<com.tns.NativeScriptActivity>;
        }
        export class NotificationAlarmReceiver extends java.lang.Object {
            public static class: java.lang.Class<com.tns.NotificationAlarmReceiver>;
        }
    }
}

@Injectable()

export class AlarmUpdater {

    cancelAndUpdateAlarm(triggerTime) {
        console.log('hnknknjhbjbhm')
        if (isAndroid) {
            var context = utils.ad.getApplicationContext();

            var alarmManager = <android.app.AlarmManager>context.getSystemService(android.content.Context.ALARM_SERVICE);
            var notificationIntent = new android.content.Intent(context, com.tns.NotificationAlarmReceiver.class)
            notificationIntent.setAction("ACTION_START_NOTIFICATION_SERVICE");

            var pendingIntent = android.app.PendingIntent.getBroadcast(context, 0, notificationIntent, android.app.PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager.cancel(pendingIntent);
            alarmManager.setRepeating(android.app.AlarmManager.RTC_WAKEUP, triggerTime, android.app.AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }
}