import { Values } from '~/app/values/values';
import { Component, OnInit, AfterContentInit, ViewChild, ElementRef, AfterViewInit } from "@angular/core";
import { TNSTextToSpeech, SpeakOptions } from 'nativescript-texttospeech';
import { UserService } from "../../services/user.service";
import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { ModalComponent } from "~/app/modals/modal.component";
import { Color } from "tns-core-modules/color/color";
import { ActivatedRoute } from "@angular/router";

import { WebView, LoadEventData } from 'tns-core-modules/ui/web-view/web-view';
import { TextField } from 'tns-core-modules/ui/text-field/text-field';

import * as Toast from "nativescript-toast"
import * as webViewInterfaceModule from 'nativescript-webview-interface';
import { PlayerService } from '~/app/services/player.service';
import { LanguageService } from '~/app/services/language/env.service';
declare var android: any;
declare const CGSizeMake: any;


@Component({
  selector: "ns-chapter",
  moduleId: module.id,
  templateUrl: "./chapter.component.html",
  styleUrls: ['./chapter.component.css']
})
export class ChapterComponent implements OnInit, AfterContentInit, AfterViewInit {

  @ViewChild('fullAccessDialog', { static: false }) fullAccessDialog: ModalComponent;
  @ViewChild('mask', { static: false }) mask: ModalComponent;
  @ViewChild('bookmarkDialog', { static: false }) bookmarkDialog: ModalComponent;
  @ViewChild('webView', { static: false }) webView: ElementRef;

  chapter: any;
  language: any;
  chapterHeading: string;
  chapterBody: string;
  chapterSubHeading: string;
  chapterSubBody: string;
  likeImage: string;
  likeStatus: boolean;
  chapterId: string;
  isWebview: boolean = true;
  speakText: string;
  isSpeaking: boolean;
  TTS: TNSTextToSpeech;
  speakOptions: SpeakOptions = {
    text: '', /// *** required ***
    speakRate: 0.5, // optional - default is 1.0
    pitch: 1.0, // optional - default is 1.0
    volume: 1.0, // optional - default is 1.0
    locale: 'en-GB', // optional - default is system locale,
  };

  quizButton: string;
  nextChapterButton: string;
  likeDialogText: string;
  buyButton: string;
  continueButton: string;
  html;
  chapterIndex: number;
  isRendering: boolean;
  renderingTimeout;

  isLoading: boolean;
  loadingTimeout;

  oWebViewInterface;
  bookmarkObj;
  bookmark;
  isMask: boolean;
  searchObj;
  search;
  page: any;


  bookmarkName: string;
  tempBookmarkEventFromWeb: any;

  bookmarkTextField: TextField;

  constructor(private languageService: LanguageService, private userService: UserService, private activatedRouter: ActivatedRoute, private routerExtensions: RouterExtensions, private playerService: PlayerService) {

    this.languageService.languageState.subscribe((res) => {
      if (res) {
        this.language = res;
      }
    }, error => {
      console.log("Error related to language service:", error)
    });
    this.language = this.languageService.langauge;

    this.isRendering = false;
    this.isLoading = false;
    this.likeImage = "res://heart_blank";
    this.likeStatus = false;
    this.chapterIndex = 0;
    this.speakText = "";
    this.isSpeaking = false;
    this.isMask = true;
    this.isWebview = true;
    this.quizButton = this.language.quizButton;
    this.nextChapterButton = this.language.nextChapterButton;
    this.likeDialogText = this.language.likeDialogText;
    this.buyButton = this.language.buyButton;
    this.continueButton = this.language.continueButton;

    this.chapterId = "";

    this.bookmarkName = "";

    this.userService.activeScreen('home');
    this.activatedRouter.queryParams.subscribe((params) => {
      if (params['chapter'] && params['chapter'] != undefined) {
        // console.log('Chapter:::', params['chapter'])
        this.chapter = JSON.parse(params['chapter']);
        this.html = this.chapter.content.url;
        if (params['index'] && params['index'] != undefined) {
          this.chapterIndex = params['index'];
          console.log('chapterIndex::', this.chapterIndex)
        }

      } else if (params['bookmark'] && params['bookmark'] != undefined) {
        this.bookmarkObj = JSON.parse(params['bookmark']);
        console.log('ChapterBookmark:::', params['bookmark'])

        this.chapter = this.bookmarkObj.chapter;
        this.bookmark = this.bookmarkObj;
        this.html = this.chapter.content.url;
        console.log('chapterIndecx:::', this.bookmarkObj.index)
        this.chapterIndex = parseInt(this.bookmarkObj.index);

      } else if (params['search'] && params['search'] != undefined) {
        this.searchObj = JSON.parse(params['search']);
        console.log('Search:::', this.searchObj)

        this.chapter = this.searchObj.chapter;
        this.search = { "value": this.searchObj.text, "nthOccurance": this.searchObj.nthOccurance }
        this.html = this.chapter.content.url;
      }
    })


    this.userService.playButtonState(true);
    this.userService.showLoadingState(false);
    this.userService.showFooter(true);

    Values.CHAPTER_NAME = this.chapter.name;
  }


  ngOnInit(): void {
    this.renderingTimeout = setTimeout(() => {
      this.isRendering = true;
    }, 300)
  }

  ngAfterContentInit(): void {

    this.renderingTimeout = setTimeout(() => {
      this.isRendering = true;
    }, 50)
  }

  ngAfterViewInit(): void {

    console.log('paymentStatus:::', this.chapter.paymentStatus)
    if (this.chapter.paymentStatus == false) {
      this.isMask = true;
      setTimeout(() => {
        this.mask.show();
      }, 2000);
    } else {
      this.isMask = false;

    }

    setTimeout(() => {
      this.isRendering = true;

      this.fullAccessDialog.show();
    }, 3000)
  }

  pageLoaded(args) {
    this.page = args.object;
    this.setupWebViewInterface(this.page)
  }

  // Initializes plugin with a webView
  setupWebViewInterface(page) {
    this.isLoading = true;
    var webView = page.getViewById('webView');

    this.oWebViewInterface = new webViewInterfaceModule.WebViewInterface(webView, this.html);
    console.log('Search:::', this.search)
    webView.on(WebView.loadFinishedEvent, (args: LoadEventData) => {
      this.isLoading = false;
      console.log('Lo:::', args.error)

      if (args.error == undefined) {
        if (this.bookmark != undefined && this.bookmark.X != undefined && this.bookmark.Y != undefined) {
          this.emitEventToWebView();
          console.log('INB')
        } else if (this.search && this.search.value != undefined && this.search.value != null && this.search.nthOccurance != undefined && this.search.nthOccurance != null) {
          this.searchInWebView();
          console.log('INS')
        }
      }
    });
    setTimeout(() => {
      this.isLoading = false;

    }, 2000)

    console.log('NAME:::', this.chapter)

    this.oWebViewInterface.on('bookmarked', (eventData) => {
      console.log('FromWeb:::', eventData, this.chapter.name)
      this.bookmarkDialog.show();
      this.tempBookmarkEventFromWeb = eventData;
    });
  }

  emitEventToWebView() {
    console.log('Bookmark Emitted', this.bookmark)
    this.oWebViewInterface.emit('scrollToPosition', this.bookmark);
  }

  searchInWebView() {
    console.log('Search Emitted', this.search)
    this.oWebViewInterface.emit('search', this.search);
  }

  protected get shadowColor(): Color {
    return new Color('#888888')
  }

  protected get shadowOffset(): number {
    return 2.0
  }

  onFullAccessDialogLoaded(args: any) {
    var dialog = <any>args.object;

    setTimeout(() => {
      if (dialog.android) {
        let nativeGridMain = dialog.android;
        var shape = new android.graphics.drawable.GradientDrawable();
        shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
        shape.setColor(android.graphics.Color.parseColor('white'));
        shape.setCornerRadius(20)
        nativeGridMain.setBackgroundDrawable(shape);
        nativeGridMain.setElevation(10)
      } else if (dialog.ios) {
        let nativeGridMain = dialog.ios;

        nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
        nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
        nativeGridMain.layer.shadowOpacity = 0.5
        nativeGridMain.layer.shadowRadius = 5.0
        nativeGridMain.layer.shadowRadius = 5.0
      }
      // this.changeDetector.detectChanges();
    }, 50)

  }

  onDialogLoaded(args: any) {
    var dialog: ModalComponent = <ModalComponent>args.object;

  }

  onBuyAccess() {
    this.isLoading = true;
    this.fullAccessDialog.hide();

    alert("Will be adding this on production")

    //Will activate in production

    // this.routerExtensions.navigate(['/payment'], {
    //   queryParams: {
    //     "chapterId": this.chapter._id
    //   }
    // })
  }

  onReadSample() {
    this.fullAccessDialog.hide();
  }

  onBookmarkTextChange(args: any) {
    this.bookmarkTextField = args.object as TextField;
    this.bookmarkName = this.bookmarkTextField.text;
  }

  onBookmarkSave() {

    if (this.bookmarkName == undefined || this.bookmarkName == "") {
      Toast.makeText('Bookmark name is required', 'long').show();
      return;
    }

    this.bookmarkTextField.dismissSoftInput();

    var bookmark: any;
    bookmark = this.tempBookmarkEventFromWeb;
    bookmark.name = this.bookmarkName;
    bookmark.chapter = this.chapter;
    bookmark.index = this.chapterIndex;

    var bookmarks = JSON.parse(Values.readString('bookmarks', '[]'));

    for (var i = 0; i < bookmarks.length; i++) {
      if (this.chapter.name == bookmarks[i].chapter.name) {
        bookmarks.splice(i, 1);
      }
    }

    console.log('Boook:::', bookmark)
    bookmarks.unshift(bookmark)

    Values.writeString('bookmarks', JSON.stringify(bookmarks))

    this.bookmarkDialog.hide();
  }

  onLikeClick() {
    if (this.likeStatus == false) {
      this.likeImage = "res://heart_fill";
      this.likeStatus = !this.likeStatus;
    }
    else {
      this.likeImage = "res://heart_blank";
      this.likeStatus = !this.likeStatus;
    }
  }

  onQuizClick() {
    this.userService.activeScreen("quiz");
    setTimeout(() => {
      this.isLoading = true;
      this.routerExtensions.navigate(['/quiz'], {
        queryParams: {
          "chapter": JSON.stringify(this.chapter)
        }
      });
    }, 10)
  }

  onNextClick() {
    // alert("next button clicked");
    this.chapterIndex++;
    var chapter = JSON.parse(Values.readString(Values.CHAPTERS, '[]'));
    console.log('NextChapter::', chapter[this.chapterIndex]);
    if (chapter[this.chapterIndex] == undefined) {
      this.chapterIndex = 0;
    }
    this.chapter = chapter[this.chapterIndex];
    this.html = this.chapter.content.url


    setTimeout(() => {
      this.setupWebViewInterface(this.page)
    }, 50);
  }

  onPlayClick() {
    Values.writeString(Values.CHAPTER_NAME, this.chapter.name)
    setTimeout(() => {
      this.playerService.toggle(this.chapter.name);

    }, 10);
  }
}