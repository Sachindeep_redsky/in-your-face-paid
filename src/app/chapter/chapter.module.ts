import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NgModalModule } from "../modals/ng-modal";
import { HttpClientModule } from "@angular/common/http";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { ChapterComponent } from "./components/chapter.component";
import { ChapterRoutingModule } from "./chapter-routing.module";


@NgModule({
    bootstrap: [
        ChapterComponent
    ],
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NgModalModule,
        HttpClientModule,
        ChapterRoutingModule,
        NgModalModule
    ],
    declarations: [
        ChapterComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})


export class ChapterModule { }
