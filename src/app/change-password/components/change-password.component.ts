import { Component, OnInit, AfterContentInit } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { TextField } from "tns-core-modules/ui/text-field";
import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { UserService } from "~/app/services/user.service";
import { HttpClient } from "@angular/common/http";
import { Values } from "~/app/values/values";
import * as LNG from "../../services/language/env.service";
import { LanguageService } from "../../services/language/env.service";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "ns-change-password",
    moduleId: module.id,
    templateUrl: "./change-password.component.html",
    styleUrls: ['./change-password.component.common.css', './change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
    language: any;
    isRendering: boolean;
    isLoading: boolean;
    oldPasswordText: string;
    newPasswordText: string;
    confirmPasswordText: string;
    changePasswordHeading: string;
    oldPasswordHint: string;
    newPasswordHint: string;
    confirmPasswordhint: string;
    updatePasswordButton: string;

    constructor(private routerExtensions: RouterExtensions, private http: HttpClient, private userService: UserService, private languageService: LanguageService) {


        this.userService.activeScreen('account');
        this.languageService.languageState.subscribe((res) => {
            if (res) {
                this.language = res;
            }
        }, error => {
            console.log("Error related to language service:", error)
        });
        this.language = this.languageService.langauge;

        this.isRendering = false;
        this.isLoading = false;
        this.oldPasswordText = "";
        this.newPasswordText = "";
        this.confirmPasswordText = "";
        this.changePasswordHeading = this.language.changePasswordHeading;
        this.oldPasswordHint = this.language.oldPasswordHint;
        this.newPasswordHint = this.language.newPasswordHint;
        this.confirmPasswordhint = this.language.confirmPasswordhint;
        this.updatePasswordButton = this.language.updatePasswordButton;
    }

    ngOnInit(): void {
        setTimeout(() => {
            this.isRendering = true;
        }, 300)
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onChangePasswordLoaded(args: any) {
        var changePasswordCard = <any>args.object;
        setTimeout(() => {
            if (changePasswordCard.android) {
                let nativeGridMain = changePasswordCard.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
            } else if (changePasswordCard.ios) {
                let nativeGridMain = changePasswordCard.ios;
                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 50)

    }

    public oldPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.oldPasswordText = textField.text;
    }

    public newPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.newPasswordText = textField.text;
    }

    public confirmPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.confirmPasswordText = textField.text;
    }

    onUpdatePassword() {

        var changePasswordModel;
        if (this.oldPasswordText == '') {
            alert('Old password can not be empty');
            return;
        }
        else if (this.newPasswordText == '') {
            alert('New password can not be empty');
            return;
        }
        else if (this.newPasswordText != this.confirmPasswordText) {
            alert('Passwords do not match');
            return;
        }
        else if (this.oldPasswordText == this.newPasswordText) {
            alert('New password can not be same as old password')
            return;
        }
        else {
            changePasswordModel = {
                password: this.oldPasswordText,
                newPassword: this.newPasswordText
            }
        }

        var headers = {
            'Content-Type': 'application/json',
            'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, "")
        }

        this.http.post(Values.BASE_URL + `users/changePassword`, changePasswordModel, { headers: headers }).subscribe((res: any) => {
            if (res && res.isSuccess) {
                if (res.data) {
                    alert(res.data);
                    setTimeout(() => {
                        Values.writeString(Values.USER, "");
                        Values.writeString(Values.X_ACCESS_TOKEN, "");
                        setTimeout(() => {
                            this.routerExtensions.navigate(['/login'], { clearHistory: true });
                        }, 10)
                    }, 10)

                    return;
                }
            }
        }, (error) => {
            // this.errorDialog.show();
            console.log('Error:', error)
            alert(error.error.error);
            if (error.error.statusCode == 401 && error.error.error == "unauthorized_user") {
                this.routerExtensions.navigate(['/login'], {
                    clearHistory: true
                });
            }
        })

        // this.routerExtensions.navigate(['/login']);
    }

}