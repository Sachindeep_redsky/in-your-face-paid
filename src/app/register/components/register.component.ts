import { Component, ViewChild, OnInit } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { TextField } from "tns-core-modules/ui/text-field";
import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { Page } from "tns-core-modules/ui/page/page";
import { UserService } from "~/app/services/user.service";
import { Values } from "~/app/values/values";
import { HttpClient } from "@angular/common/http";
import { ModalComponent } from "~/app/modals/modal.component";
import { GridLayout } from "tns-core-modules/ui/layouts/grid-layout/grid-layout";
import * as LNG from "../../services/language/env.service";
import { LanguageService } from "../../services/language/env.service";


declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "ns-register",
    moduleId: module.id,
    templateUrl: "./register.component.html",
    styleUrls: ['./register.component.common.css', './register.component.css']
})
export class RegisterComponent implements OnInit {

    @ViewChild('errorDialog', { static: false }) errorDialog: ModalComponent;
    language: any;
    isRendering: boolean;
    isLoading: boolean;
    nameText: string;
    passwordText: string;
    confirmPasswordText: string;
    emailText: string;
    phoneText: string;
    registerHeading: string;
    nameHint: string;
    passwordHint: string;
    emailHint: string;
    tryAgain: string;
    somethingWentWrong: string;
    // phoneNumberHint: string;
    confirmPasswordhint: string;
    signUpButton: string;
    alreadyAccount: string;
    login: string;
    constructor(private languageService: LanguageService, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient, private page: Page) {

        this.languageService.languageState.subscribe((res) => {
            if (res) {
                this.language = res;
            }
        }, error => {
            console.log("Error related to language service:", error)
        })
        this.language = this.languageService.langauge;

        this.isRendering = false;
        this.isLoading = false;
        this.nameText = "";
        this.passwordText = "";
        this.confirmPasswordText = "";
        this.emailText = "";
        this.phoneText = "";
        this.confirmPasswordText = "";
        this.emailText = "";
        this.phoneText = "";
        this.tryAgain = this.language.tryAgain;
        this.somethingWentWrong = this.language.somethingWentWrong;
        this.registerHeading = this.language.registerHeading;
        this.nameHint = this.language.nameHint;
        this.passwordHint = this.language.passwordHint;
        this.emailHint = this.language.onlyemailHint;
        // this.phoneNumberHint = "Phone number";
        this.confirmPasswordhint = this.language.confirmPasswordhint;
        this.signUpButton = this.language.registerHeading;
        this.alreadyAccount = this.language.alreadyAccount;
        this.login = this.language.loginHeading;
        this.userService.showFooter(false);
    }

    ngOnInit(): void {
        setTimeout(() => {
            this.isRendering = true;
        }, 300)
    }


    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onRegisterLoaded(args: any) {
        var registerCard = <any>args.object;
        setTimeout(() => {
            if (registerCard.android) {
                let nativeGridMain = registerCard.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
            } else if (registerCard.ios) {
                let nativeGridMain = registerCard.ios;
                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
        }, 50)
    }

    onErrorDialogLoaded(args: any) {
        var dialog = <GridLayout>args.object;

        setTimeout(() => {
            if (dialog.android) {
                let nativeGridMain = dialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(30)
            } else if (dialog.ios) {
                let nativeGridMain = dialog.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
        }, 5)

    }

    public nameTextField(args) {
        var textField = <TextField>args.object;
        this.nameText = textField.text;
    }

    public emailTextField(args) {
        var textField = <TextField>args.object;
        this.emailText = textField.text;
    }

    // public phoneTextField(args) {
    //     var textField = <TextField>args.object;
    //     this.phoneText = textField.text;
    // }

    public passwordTextField(args) {
        var textField = <TextField>args.object;
        this.passwordText = textField.text;
    }

    public confirmPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.confirmPasswordText = textField.text;
    }

    onDialogHide() {
        this.errorDialog.hide();
    }

    onLoginClick() {
        this.routerExtensions.back();
    }

    onRegisterClick() {
        console.log('SignUp tapped')
        if (this.nameText == "") {
            alert("Please enter username.");
        }
        else if (this.emailText == "") {
            alert("Please enter email.");
        }
        // else if (this.phoneText == "") {
        //     alert("Please enter phone number.");
        // }
        // else if (this.phoneText.length < 10) {
        //     alert("Phone number should be of ten digits.");
        // }
        else if (this.passwordText == "") {
            alert("Please enter password.");
        }
        else if (this.passwordText.length < 6) {
            alert("Password should be at least 6 digits")
        }
        else if (this.confirmPasswordText == "") {
            alert("Please enter confirm password.");
        }
        else if (this.passwordText != this.confirmPasswordText) {
            alert("Password and confirm password should be same.");
        }
        else {
            var user = {
                name: this.nameText,
                email: this.emailText,
                // phone: this.phoneText,
                password: this.passwordText
            }

            var headers = {
                'Content-Type': 'application/json'
            }

            this.http.post(Values.BASE_URL + `users`, user, { headers: headers }).subscribe((res: any) => {
                if (res && res.isSuccess && res.data) {
                    console.log('Response:', res);
                    console.log('Token:', res.data.regToken)
                    this.routerExtensions.navigate(['/confirmOtp'], {
                        queryParams: {
                            "fromForgotPassword": false,
                            "email": this.emailText
                        }
                    })
                }
            }, (error) => {
                console.log('Error:', error);
                alert('Something went wrong');
            })
        }
    }

}