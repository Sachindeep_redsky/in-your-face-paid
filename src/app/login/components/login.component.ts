import { Component, OnInit } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { TextField } from "tns-core-modules/ui/text-field";
import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { UserService } from "~/app/services/user.service";
import { HttpClient } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { LanguageService } from "../../services/language/env.service";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "ns-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ['./login.component.common.css', './login.component.css']
})
export class LoginComponent implements OnInit {
    language: any;
    isRendering: boolean;
    isLoading: boolean;
    emailText: string;
    passwordText: string;
    loginHeading: string;
    usernameHint: string;
    passwordHint: string;
    dontAccount: string;
    createNow: string;
    loginButton: string;
    forgotPassword: string;

    constructor(private languageService: LanguageService, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient) {
        this.languageService.languageState.subscribe((res) => {
            if (res) {
                this.language = res;
            }
        }, error => {
            console.log("Error related to language service:", error)
        })
        this.language = this.languageService.langauge;
        this.isRendering = false;
        this.isLoading = false;
        this.emailText = "";
        this.passwordText = "";
        this.loginHeading = this.language.loginHeading;
        this.usernameHint = this.language.usernameHint;
        this.passwordHint = this.language.passwordHint;
        this.dontAccount = this.language.dontAccount;
        this.createNow = this.language.createNow;
        this.loginButton = this.language.loginButton;
        this.forgotPassword = this.language.forgotHeading + '?';
        this.userService.showFooter(false);
        console.log("Login")
    }

    ngOnInit(): void {
        setTimeout(() => {
            this.isRendering = true;
        }, 300)
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onLoginLoaded(args: any) {
        var loginCard = <any>args.object;
        setTimeout(() => {
            if (loginCard.android) {
                let nativeGridMain = loginCard.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
            } else if (loginCard.ios) {
                let layer = loginCard.ios.layer;

                layer.backgroundColor = UIColor.whiteColor.CGColor;
                // layer.shadowColor = new Color('#000000').ios.CGColor;
                layer.shadowOffset = CGSizeMake(0, 1);
                layer.shadowOpacity = 1;
                layer.shadowRadius = 5.0;
                layer.cornerRadius = 20;
                // nativeGridMain.layer.cornerRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 50)

    }

    public userNameTextField(args) {
        var textField = <TextField>args.object;
        this.emailText = textField.text;
    }

    public passwordTextField(args) {
        var textField = <TextField>args.object;
        this.passwordText = textField.text;
    }

    onLoginClick() {
        console.log('Login tapped')
        if (this.emailText == "") {
            alert("Please enter username.");
        }
        else if (this.passwordText == "") {
            alert("Please enter password.");
        }
        else if (this.passwordText.length < 6) {
            alert("Password should be at least 6 digits")
        }
        else {
            var user = {
                email: this.emailText,
                password: this.passwordText
            }

            var headers = {
                'Content-Type': 'application/json'
            }

            this.http.post(Values.BASE_URL + `users/login`, user, { headers: headers }).subscribe((res: any) => {
                if (res && res.isSuccess) {
                    console.trace('Response:', res);
                    Values.writeString(Values.USER, JSON.stringify(res.data));
                    Values.writeString(Values.X_ACCESS_TOKEN, res.data.token);
                    this.routerExtensions.navigate(['/home'], { clearHistory: true })
                }
            }, (error) => {
                console.log('Error:', error);
                alert(error.error.error);
                Values.writeString(Values.USER, "");
            })
        }
    }

    onRegisterClick() {
        console.log('Registration tapped')
        this.routerExtensions.navigate(['/register']);
    }

    onForgotPassword() {
        console.log('Forgot tapped')
        this.routerExtensions.navigate(['/forgotPassword']);
    }

}