import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NgModalModule } from "../modals/ng-modal";
import { HttpClientModule } from "@angular/common/http";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { ConfirmOtpComponent } from "./components/confirm-otp.component";
import { ConfirmOtpRoutingModule } from "./confirm-otp-routing.module";


@NgModule({
    bootstrap: [
        ConfirmOtpComponent
    ],
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NgModalModule,
        HttpClientModule,
        ConfirmOtpRoutingModule,
    ],
    declarations: [
        ConfirmOtpComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})


export class ConfirmOtpModule { }
