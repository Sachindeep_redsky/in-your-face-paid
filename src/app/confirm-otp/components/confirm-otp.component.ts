import { Component, OnInit } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { TextField } from "tns-core-modules/ui/text-field";
import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { UserService } from "~/app/services/user.service";
import { Page } from "tns-core-modules/ui/page/page";
import { Values } from "~/app/values/values";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, RouteReuseStrategy } from "@angular/router";
import { LanguageService } from "../../services/language/env.service";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "ns-confirmOtp",
    moduleId: module.id,
    templateUrl: "./confirm-otp.component.html",
    styleUrls: ['./confirm-otp.component.common.css', './confirm-otp.component.css']
})
export class ConfirmOtpComponent implements OnInit {
    language: any;;
    isRendering: boolean;
    isLoading: boolean;
    otpText: string;
    confirmOtpHeading: string;
    otpHint: string;
    confirmButton: string;
    isFromForgotPassword: string;
    showPassword: boolean;
    passwordText: string;
    confirmPasswordText: string;
    email: string;
    height: number;

    constructor(private languageService: LanguageService, private routerExtensions: RouterExtensions, private activatedRouter: ActivatedRoute, private userService: UserService, private http: HttpClient, private page: Page) {
        this.languageService.languageState.subscribe((res) => {
            if (res) {
                this.language = res;
            }
        }, error => {
            console.log("Error related to language service:", error)
        })
        this.language = this.languageService.langauge;

        this.isRendering = false;
        this.isLoading = false;
        this.isFromForgotPassword = 'false';
        this.otpText = "";
        this.confirmOtpHeading = this.language.confirmOtpHeading;
        this.otpHint = this.language.otpHint;
        this.confirmButton = this.language.confirmButton;
        this.showPassword = false;
        this.passwordText = "";
        this.confirmPasswordText = "";


        this.activatedRouter.queryParams.subscribe(params => {
            console.trace('Params', params);
            console.trace('Params', params.fromForgotPassword);
            console.trace('Params', params.token);
            if (params["fromForgotPassword"] != undefined) {
                this.isFromForgotPassword = params["fromForgotPassword"]
                if (this.isFromForgotPassword == 'true') {
                    this.height = 65;
                    this.showPassword = true;
                } else {
                    this.showPassword = false;
                }
            }
            if (params["email"] != undefined && params["email"] != null && params["email"] != "") {
                this.email = params["email"];
            }
        })
        this.userService.showFooter(false);
    }

    ngOnInit(): void {
        setTimeout(() => {
            this.isRendering = true;
        }, 300)
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onForgotPasswordLoaded(args: any) {
        var forgotPasswordCard = <any>args.object;
        setTimeout(() => {
            if (forgotPasswordCard.android) {
                let nativeGridMain = forgotPasswordCard.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
            } else if (forgotPasswordCard.ios) {
                let nativeGridMain = forgotPasswordCard.ios;
                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
        }, 50)

    }

    public otpTextField(args) {
        var textField = <TextField>args.object;
        this.otpText = textField.text;
    }

    public passwordTextField(args) {
        var textField = <TextField>args.object;
        this.passwordText = textField.text;
    }

    public confirmPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.confirmPasswordText = textField.text;
    }

    onConfirmClick() {
        var otpModel
        if (this.showPassword) {
            if (this.passwordText == '') {
                alert('Password can not be empty');
                return;
            }
            else if (this.passwordText != this.confirmPasswordText) {
                alert('Passwords do not match')
                return;
            }
            else {
                otpModel = {
                    email: this.email,
                    otp: this.otpText,
                    newPassword: this.passwordText
                }
            }
        } else {
            otpModel = {
                email: this.email,
                otp: this.otpText
            }
        }


        var route: string;
        if (this.isFromForgotPassword == 'true') {
            route = 'resetPassword';;
        } else {
            route = 'verifyUser';
        }
        console.log('Route:', route)


        var headers = {
            'Content-Type': 'application/json',
        }

        this.http.post(Values.BASE_URL + `users/${route}`, otpModel, { headers: headers }).subscribe((res: any) => {
            if (res && res.isSuccess) {
                if (res.data && res.data.token && res.data.token != undefined && res.data.token != null) {
                    Values.writeString(Values.USER, JSON.stringify(res.data));
                    Values.writeString(Values.X_ACCESS_TOKEN, res.data.token);
                    this.routerExtensions.navigate(['/home'], { clearHistory: true });
                    return;
                }
                console.log('Response:', res)
                this.routerExtensions.navigate(['/login'], { clearHistory: true });
            }
        }, (error) => {
            // this.errorDialog.show();
            console.log('Error:', error)
            alert(error.error.error);
            if (error.error.statusCode == 401 && error.error.error == "unauthorized_user") {
                this.routerExtensions.navigate(['/login'], {
                    clearHistory: true
                });
            }
        })
    }

    onBack() {
        this.routerExtensions.back();
    }

}