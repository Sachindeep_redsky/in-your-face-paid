export class User {

    id: string;
    name: string;
    email: string;
    phone: number;

    constructor(obj?) {
        this.id = obj.id;
        this.name = obj.name;
        this.email = obj.email;
        this.phone = obj.phone;
    }

}