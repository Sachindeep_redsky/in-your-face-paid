import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NgModalModule } from "../modals/ng-modal";
import { HttpClientModule } from "@angular/common/http";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { QuizComponent } from "./components/quiz.component";
import { QuizRoutingModule } from "./quiz-routing.module";


@NgModule({
    bootstrap: [
        QuizComponent
    ],
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NgModalModule,
        HttpClientModule,
        QuizRoutingModule
    ],
    declarations: [
        QuizComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})


export class QuizModule { }
