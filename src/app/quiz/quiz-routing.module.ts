import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { QuizComponent } from "./components/quiz.component";

const routes: Routes = [
    // { path: "", redirectTo: "/quiz", pathMatch: "full" },
    // { path: "quiz", component: QuizComponent },
    { path: "", component: QuizComponent },
];


@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})


export class QuizRoutingModule { }
