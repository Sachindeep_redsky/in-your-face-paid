import { Component, OnInit, AfterContentInit } from "@angular/core";
import { RouterExtensions } from 'nativescript-angular/router/router-extensions'
import { UserService } from "../../services/user.service";
import { Page } from "tns-core-modules/ui/page/page";
import { Values } from "~/app/values/values";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute } from "@angular/router";
import { LanguageService } from "../../services/language/env.service";

@Component({
    selector: "ns-quiz",
    moduleId: module.id,
    templateUrl: "./quiz.component.html",
    styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit, AfterContentInit {

    language: any;
    selectedOption: number;
    selectedOptionIcon = "res://box_white";
    unselectedOptionIcon = "res://box_grey";

    selectedOptionColor = "#273BD3";
    unselectedOptionColor = "white";

    selectedTextColor = "white";
    unselectedTextColor = "black";
    question: string;
    questions;
    result;
    questionName = "";
    questionNumber: number = 1;
    progressValue: number = 10;
    isVisibleQuizResult: boolean;
    isVisibleQuiz: boolean;

    isRendering: boolean;
    renderingTimeout;

    isLoading: boolean;
    loadingTimeout;

    chapter: any;

    questionOrigin: string;
    option1: string;
    option2: string;
    option3: string;
    option4: string;


    constructor(private languageService: LanguageService, private activatedRouter: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private page: Page, private http: HttpClient) {
        this.languageService.languageState.subscribe((res) => {
            if (res) {
                this.language = res;
            }
        }, error => {
            console.log("Error related to language service:", error)
        })
        this.language = this.languageService.langauge;

        this.isRendering = false;
        this.isLoading = false;

        this.userService.activeScreen('quiz');
        this.userService.playButtonState(false);
        this.userService.showLoadingState(false);
        this.userService.showFooter(true);
        this.questions = [];
        this.question = this.language.question;
        this.selectedOption = -1;
        this.option1 = "Option 1";
        this.option2 = "Option 2";
        this.option3 = "Option 3";
        this.option4 = "Option 4";
        this.result = {};

        this.activatedRouter.queryParams.subscribe(params => {
            if (params["chapter"] && params["chapter"] != undefined && params["chapter"] != null) {
                this.chapter = JSON.parse(params["chapter"]);
            }
        })

        if (!this.chapter) {
            var chapters = JSON.parse(Values.readString(Values.CHAPTERS, "[]"));
            if (chapters && chapters.length != 0 && chapters[0]) {
                this.chapter = chapters[0];
            }
        }

        this.questionOrigin = this.chapter.name;

        this.result.chapterId = this.chapter._id;
        this.result.answers = [];
    }

    ngOnInit(): void {

        this.renderingTimeout = setTimeout(() => {
            this.getQuiz();
            this.isRendering = true;
        }, 300)
    }

    ngAfterContentInit(): void {
    }

    getQuiz() {
        this.questions = [];
        var headers = {
            'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ""),
            'Content-Type': 'application/json'
        }

        this.http.get(Values.BASE_URL + `quiz?id=${this.chapter._id}`, { headers: headers }).subscribe((res: any) => {
            console.trace('RES:::', res)
            if (res && res.isSuccess && res.data) {
                for (var i = 0; i < res.data.length; i++) {
                    this.questions.push(res.data[i])
                }
                console.trace('QUES:::', this.questions)

                this.questionName = this.questions[0].question;
                this.option1 = this.questions[0].options[0].value;
                this.option2 = this.questions[0].options[1].value;
                this.option3 = this.questions[0].options[2].value;
                this.option4 = this.questions[0].options[3].value;
            }
        }, error => {
            console.log('Error:::', error)
            if (error.error.statusCode == 401 && error.error.error == "unauthorized_user") {
                this.routerExtensions.navigate(['/login'], {
                    clearHistory: true
                });
            }
        })
    }

    getQuizResult() {
        var results = [];
        results = this.questions;

        var correctAnswers: number = 0;

        var headers = {
            'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ""),
            'Content-Type': 'application/json'
        }

        this.http.post(Values.BASE_URL + `results`, this.result, { headers: headers }).subscribe((res: any) => {
            console.trace('RES:::', res)
            if (res && res.isSuccess && res.data && res.data.answers) {
                for (var i = 0; i < res.data.answers.length; i++) {
                    for (var j = 0; j < this.questions.length; j++) {
                        if (res.data.answers[i].questionId == this.questions[j]._id) {
                            results[j].answerId = res.data.answers[i].answerId;
                        }
                    }
                }

                for (var i = 0; i < this.result.answers.length; i++) {
                    for (var j = 0; j < results.length; j++) {
                        if (results[j]._id == this.result.answers[i].questionId) {
                            results[j].selectedOptionId = this.result.answers[i].answerId;
                        }
                    }
                }

                for (var k = 0; k < res.data.answers.length; k++) {
                    if (res.data.answers[k].isCorrect) {
                        correctAnswers = correctAnswers + 1;
                    }
                }

                setTimeout(() => {
                    this.isLoading = true;
                    this.routerExtensions.navigate(['/quizResult'], {
                        queryParams: {
                            "result": JSON.stringify(results),
                            "quizPercentage": res.data.quizPercentage,
                            "correctAnswers": correctAnswers,
                            "chapter": JSON.stringify(this.chapter)
                        }, clearHistory: true
                    });
                }, 10)

                this.routerExtensions.navigate(['/quizResult'], {
                    queryParams: {
                        "result": JSON.stringify(results),
                        "quizPercentage": res.data.quizPercentage,
                        "correctAnswers": correctAnswers,
                        "chapter": JSON.stringify(this.chapter)
                    }, clearHistory: true
                });
            }
        }, error => {
            console.log('Error:::', error)
            if (error.error.statusCode == 401 && error.error.error == "unauthorized_user") {
                this.routerExtensions.navigate(['/login'], {
                    clearHistory: true
                });
            }
        })
    }

    showQuestion() {
        this.userService.showLoadingState(false);
        this.selectedOption = -1;
        this.questionNumber = this.questionNumber + 1;
        this.progressValue = this.questionNumber * 10;
        this.questionName = this.questions[this.questionNumber - 1].question;
        this.option1 = this.questions[this.questionNumber - 1].options[0].value;
        this.option2 = this.questions[this.questionNumber - 1].options[1].value;
        this.option3 = this.questions[this.questionNumber - 1].options[2].value;
        this.option4 = this.questions[this.questionNumber - 1].options[3].value;
    }

    onValueChanged(args) {
    }

    onOptionClick(index: number) {

        console.log("INDEX", index)
        this.result.answers.push({ "questionId": this.questions[this.questionNumber - 1]._id, "answerId": this.questions[this.questionNumber - 1].options[index]._id })
        this.selectedOption = index;
        setTimeout(() => {
            if (this.questionNumber == 10) {
                this.getQuizResult();
                // this.routerExtensions.navigate(['./home']);
            } else {
                this.showQuestion();
            }
        }, 50);
    }

}
