import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NgModalModule } from "./modals/ng-modal";
import { HttpClientModule } from "@angular/common/http";
import { UserService } from "./services/user.service";
import { AuthService } from "./services/auth.service";
import { AuthGuard } from "./services/guards/auth.guard.service";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { PaymentComponent } from "./payment/payment.component";
import { NativeScriptBottomNavigationBarModule } from "nativescript-bottom-navigation/angular";
import { RouteReuseStrategy } from "@angular/router";
import { CacheRouteReuseStrategy } from "./services/cache-route-reuse.strategy";
import { PagerModule } from "nativescript-pager/angular";
import { ReminderService } from "./services/reminder.service";
import { PlayerService } from "./services/player.service";
import { AlarmUpdater } from "./services/alarm/alarm.updater";
import { LanguageService } from "./services/language/env.service";
import { DropDownModule } from "nativescript-drop-down/angular";


@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NgModalModule,
        HttpClientModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptBottomNavigationBarModule,
        PagerModule,
        DropDownModule
    ],
    declarations: [
        AppComponent, PaymentComponent
    ],
    providers: [UserService, AuthService, ReminderService, AuthGuard, PlayerService, AlarmUpdater, LanguageService, {
        provide: RouteReuseStrategy,
        useClass: CacheRouteReuseStrategy
    }],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})


export class AppModule { }
