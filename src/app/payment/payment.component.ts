import { Component, OnInit, AfterContentInit, AfterViewInit } from "@angular/core";
import { Braintree, BrainTreeOptions } from 'nativescript-braintree';
import { HttpClient } from "@angular/common/http";
import { Values } from "../values/values";
import { RouterExtensions } from "nativescript-angular/router/router-extensions";


@Component({
    selector: "ns-payment",
    templateUrl: "./payment.component.html",
    styleUrls: ['./payment.component.css']
})

export class PaymentComponent implements OnInit, AfterContentInit, AfterViewInit {

    token;
    braintree;

    options: BrainTreeOptions;
    isRendering: boolean;
    renderingTimeout;

    constructor(private http: HttpClient, private routerExtensions: RouterExtensions) {
        this.isRendering = false;
        this.token = "ghghhhhh";

        this.braintree = new Braintree();

        this.options = {
            amount: "10.0",
            collectDeviceData: true,
            requestThreeDSecureVerification: false,

        }

        var headers = {
            'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ""),
            'Content-Type': 'application/json'
        }

        this.http.get(Values.BASE_URL + `payments/token`, { headers: headers }).subscribe((res: any) => {
            if (res && res.isSuccess && res.data && res.data.token) {
                console.log('PAYMENT:::TOKEN:::', res)
                this.token = res.data.token;
                this.braintree.startPayment(this.token, this.options);
            }
        }, error => {
            alert('Error: Could not get token from server');
            console.log('Error:', error)
        })

        this.braintree.on("success", (res) => {
            let output: any = res.object.get("output");
            console.dir(output);
            if (output && output.nonce) {
                this.checkoutWithNonce(output.nonce)
            } else {
                alert('Could not get nonce from server')
            }
        })

        this.braintree.on("cancel", (res) => {
            let output = res.object.get("output");
            console.dir(output);
        })

        this.braintree.on("error", (res) => {
            let output = res.object.get("output");
            console.dir(output);
        })
    }

    ngOnInit(): void {
    }

    ngAfterContentInit(): void {
        this.renderingTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 50)
    }

    ngAfterViewInit(): void {
    }

    checkoutWithNonce(nonce: any) {
        var headers = {
            'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ""),
            'Content-Type': 'application/json'
        }

        this.http.post(Values.BASE_URL + `payments/checkout`, { "nonce": nonce }, { headers: headers }).subscribe((res: any) => {
            if (res && res.isSuccess) {
                console.log('PAYMENT:::CHECKOUT:::', res);
                this.routerExtensions.navigate(['/home']);
            }
        }, error => {
            console.log('Error: Could not checkout with nonce', error);
            if (error.error.statusCode == 401 && error.error.error == "unauthorized_user") {
                this.routerExtensions.navigate(['/login'], {
                    clearHistory: true
                });
            }
        })

    }

}