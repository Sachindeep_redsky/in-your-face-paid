import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NgModalModule } from "../modals/ng-modal";
import { HttpClientModule } from "@angular/common/http";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { QuizResultComponent } from "./components/quiz-result.component";
import { QuizResultRoutingModule } from "./quiz-result-routing.module";
import { PagerModule } from "nativescript-pager/angular";


@NgModule({
    bootstrap: [
        QuizResultComponent
    ],
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NgModalModule,
        HttpClientModule,
        QuizResultRoutingModule,
        PagerModule
    ],
    declarations: [
        QuizResultComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})


export class QuizResultModule { }
