import { Component, OnInit, AfterContentInit } from "@angular/core";
import { RouterExtensions } from 'nativescript-angular/router/router-extensions'
import { UserService } from "../../services/user.service";
import { Page } from "tns-core-modules/ui/page/page";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: "ns-quizResult",
    moduleId: module.id,
    templateUrl: "./quiz-result.component.html",
    styleUrls: ['./quiz-result.component.css']
})
export class QuizResultComponent implements OnInit, AfterContentInit {

    questions;
    questionStatus: any = 0;
    questionNumber: any = 1;
    correctAnswers: number = 0;
    quizPercentage: any = 0;
    selectedPage: number = 0;
    nextButtonText: string;

    result;
    chapter: any;

    isRendering: boolean;
    renderingTimeout;

    isLoading: boolean;
    loadingTimeout;

    option1: string;
    option2: string;
    option3: string;
    option4: string;

    constructor(private routerExtensions: RouterExtensions, private activatedRoute: ActivatedRoute, private userService: UserService, private page: Page) {
        this.isRendering = false;
        this.isLoading = false;

        this.userService.activeScreen('quiz');
        this.userService.playButtonState(false);
        this.userService.showLoadingState(false);
        this.userService.showFooter(true);
        this.questions = [];

        this.nextButtonText = "Next"

        this.activatedRoute.queryParams.subscribe(params => {
            if (params["result"] && params["result"] != undefined && params["result"] != null) {
                this.result = JSON.parse(params["result"]);
                console.trace('Q-R:::', this.result)
            }
            if (params["correctAnswers"] && params["correctAnswers"] != undefined && params["correctAnswers"] != null) {
                this.correctAnswers = JSON.parse(params["correctAnswers"]);
            }
            if (params["quizPercentage"] && params["quizPercentage"] != undefined && params["quizPercentage"] != null) {
                this.quizPercentage = JSON.parse(params["quizPercentage"]);
            }
            if (params["chapter"] && params["chapter"] != undefined && params["chapter"] != null) {
                this.chapter = JSON.parse(params["chapter"]);
            }
        })


    }

    ngOnInit(): void {
        // this.questions.push({ questionName: "Sed do eiusmod tempor incididunt labore et dolore magna aliqua?", checked: 1, right: 2 });
        // this.questions.push({ questionName: "Question 2", checked: 2, right: 1 });
        // this.questions.push({ questionName: "Question 3", checked: 2, right: 2 });
        // this.questions.push({ questionName: "Question 4", checked: 1, right: 3 });
        // this.questions.push({ questionName: "Question 5", checked: 3, right: 1 });
        // this.questions.push({ questionName: "Question 6", checked: 3, right: 3 });
        // this.questions.push({ questionName: "Question 7", checked: 1, right: 4 });
        // this.questions.push({ questionName: "Question 8", checked: 1, right: 1 });
        // this.questions.push({ questionName: "Question 9", checked: 4, right: 4 });
        // this.questions.push({ questionName: "Question 10", checked: 4, right: 2 });

        // this.firstBorderColor = "#9E9E9E";
        // this.secondBorderColor = "#9E9E9E";
        // this.thirdBorderColor = "#9E9E9E";
        // this.fourthBorderColor = "#9E9E9E";

        // for (let i = 0; i < this.questions.length; i++) {
        //     if (this.questions[i].right == this.questions[i].checked) {
        //         this.correctQuestion++;
        //     }
        // }

        this.renderingTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 300)

        this.quizPercentage = this.correctAnswers * 10;

        // this.showOptionColor(0);
    }

    ngAfterContentInit(): void {

    }

    onIndexChanged(args: any) {

    }

    onNextButtonTap(index: number) {

        // this.selectedPage++;

        // this.firstBorderColor = "#9E9E9E";
        // this.secondBorderColor = "#9E9E9E";
        // this.thirdBorderColor = "#9E9E9E";
        // this.fourthBorderColor = "#9E9E9E";
        // let i: any = 0;
        if (this.selectedPage < 9) {
            if (this.selectedPage + 1 == 9) {
                this.nextButtonText = "Go to source";
            } else {
                this.nextButtonText = "Next";
            }
            this.selectedPage++;
            this.questionNumber = this.selectedPage + 1;
        }
        else if (this.selectedPage == 9) {


            setTimeout(() => {
                this.isLoading = true;
                this.routerExtensions.navigate(['/chapter'], {
                    queryParams: {
                        "chapter": JSON.stringify(this.chapter)
                    }, clearHistory: true
                })
            }, 10)
            // this.questionNumber = this.selectedPage + 1;
        }

        // this.showOptionColor(this.selectedPage);
    }

    // showOptionColor(i: any) {
    //     if (this.questions[i].right == this.questions[i].checked) {
    //         this.nextButtonText = "Next"
    //         if (this.questions[i].right == 1) {
    //             this.firstBorderColor = "#16BF1E";
    //         }
    //         if (this.questions[i].right == 2) {
    //             this.secondBorderColor = "#16BF1E";
    //         }
    //         if (this.questions[i].right == 3) {
    //             this.thirdBorderColor = "#16BF1E";
    //         }
    //         if (this.questions[i].right == 4) {
    //             this.fourthBorderColor = "#16BF1E";
    //         }
    //     }
    //     else {
    //         this.nextButtonText = "Go to source";
    //         if (this.questions[i].right == 1) {
    //             this.firstBorderColor = "#16BE1E";
    //         }
    //         if (this.questions[i].right == 2) {
    //             this.secondBorderColor = "#16BE1E";
    //         }
    //         if (this.questions[i].right == 3) {
    //             this.thirdBorderColor = "#16BE1E";
    //         }
    //         if (this.questions[i].right == 4) {
    //             this.fourthBorderColor = "#16BE1E";
    //         }
    //         if (this.questions[i].checked == 1) {
    //             this.firstBorderColor = "#D42727";
    //         }
    //         if (this.questions[i].checked == 2) {
    //             this.secondBorderColor = "#D42727";
    //         }
    //         if (this.questions[i].checked == 3) {
    //             this.thirdBorderColor = "#D42727";
    //         }
    //         if (this.questions[i].checked == 4) {
    //             this.fourthBorderColor = "#D42727";
    //         }
    //     }
    // }

    myChangeEvent(args) {
        this.selectedPage = args.index;

        let i: any = 0;
        if (this.selectedPage < 9) {
            // i = this.selectedPage;
            // this.selectedPage++;
            // this.questionNumber++;
            this.questionNumber = this.selectedPage + 1;
        }
        if (this.selectedPage == 9) {
            this.questionNumber = this.selectedPage + 1;
        }

        // this.showOptionColor(this.selectedPage);
    }
}
