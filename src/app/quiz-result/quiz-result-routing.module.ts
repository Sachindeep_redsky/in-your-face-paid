import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { QuizResultComponent } from "./components/quiz-result.component";

const routes: Routes = [
    { path: "", component: QuizResultComponent },
];


@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})


export class QuizResultRoutingModule { }
