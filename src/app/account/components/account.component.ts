import { Color } from 'tns-core-modules/color/color';
import { RouterExtensions } from 'nativescript-angular/router';
import { Values } from './../../values/values';
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, ChangeDetectorRef } from "@angular/core";
import { UserService } from "../../services/user.service";
import { Menu } from "nativescript-menu";
import { ModalComponent } from "~/app/modals/modal.component";
import { TimePicker } from "tns-core-modules/ui/time-picker/time-picker";
import { ReminderService } from '~/app/services/reminder.service';
import { TextField } from 'tns-core-modules/ui/text-field/text-field';
import { HttpClient } from '@angular/common/http';
import { ImageAsset } from 'tns-core-modules/image-asset/image-asset';
import { session, ResultEventData } from 'nativescript-background-http';

import * as Imagepicker from "nativescript-imagepicker";
import { isAndroid } from 'tns-core-modules/platform/platform';
import { LanguageService } from '../../services/language/env.service';

declare const CGSizeMake: any;

@Component({
    selector: "ns-account",
    moduleId: module.id,
    templateUrl: "./account.component.html",
    styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit, AfterViewInit {

    @ViewChild('reminderDialog', { static: false }) reminderDialog: ModalComponent;
    @ViewChild('nameTextField', { static: false }) nameTextFieldRef: ElementRef;
    @ViewChild('languageSelectDialog', { static: false }) languageSelectDialog: ModalComponent;

    language: any;
    quizDetails: string;
    userName = "Saulius";
    percentage = "80%";
    number = "08";
    attempted = "21";
    total = "34";
    menu: Menu;
    activityText: string;
    eiusmodDo: string;
    selectedCard: number;
    isRendering: boolean;
    renderingTimeout;
    typeBioHere: string;
    isLoading: boolean;
    loadingTimeout;
    selectedDays;
    editButton: string;
    chapters;
    nameBorderColor: string;
    statusBorderColor: string;
    chapterText: string;
    isEditable: boolean;

    updateBody: { name: string; profile: { image: { resize_url: string }; bio: string } };

    nameText: string;
    statusText: string;
    imageUrl: string;
    languageList: any;
    showUpdateButtons: boolean;

    user: any;

    context;

    nameTextField: any;
    nameIsFocussed: boolean;
    statusIsFocussed: boolean;
    chaptersNew;

    isActivityLoading: boolean;
    isQuizDetailLoading: boolean;

    constructor(private routerExtensions: RouterExtensions, private userService: UserService, private router: RouterExtensions, private reminderService: ReminderService, private http: HttpClient, private changeDetector: ChangeDetectorRef, private languageService: LanguageService) {
        this.languageService.languageState.subscribe((res) => {
            if (res) {
                this.language = res;

                setTimeout(() => {
                    this.routerExtensions.navigate(['']);

                }, 100);
            }
        }, error => {
            console.log("Error related to language service:", error)
        })
        this.language = this.languageService.langauge;

        this.isRendering = false;
        this.isLoading = false;
        this.userService.activeScreen('account');
        this.userService.playButtonState(false);
        this.userService.showLoadingState(false);
        this.userService.showFooter(true);
        this.activityText = this.language.activityText;
        this.eiusmodDo = this.language.eiusmodDo;
        this.quizDetails = this.language.quizDetails;
        this.selectedCard = -1;
        this.selectedDays = [false, false, false, false, false, false, false];
        this.isEditable = false;
        this.showUpdateButtons = false;
        this.typeBioHere = this.language.typeBioHere;
        this.editButton = this.language.editButton;
        this.chapterText = this.language.chapterText
        this.nameBorderColor = "transparent";
        this.statusBorderColor = "transparent";
        this.languageList = this.languageService.langaugeList
        this.nameIsFocussed = false;
        this.statusIsFocussed = false;

        this.isActivityLoading = false;
        this.isQuizDetailLoading = false;

        this.context = Imagepicker.create({
            mode: "single",
            mediaType: Imagepicker.ImagePickerMediaType.Image
        });

        if (Values.readString(Values.USER, "") != "") {
            this.user = JSON.parse(Values.readString(Values.USER, "{}"));
            if (this.user.name) {
                this.nameText = this.user.name;
                this.userName = this.user.name;
            }
            else {
                this.nameText = "";
            }
            if (this.user.profile.bio) {
                this.statusText = this.user.profile.bio;
            }
            else {
                this.statusText = "";
            }
            if (this.user.profile && this.user.profile.image && this.user.profile.image.resize_url) {
                this.imageUrl = this.user.profile.image.resize_url;
            } else {
                this.imageUrl = "res://profile";
            }
        } else {
            this.nameText = "";
            this.statusText = "";
            this.imageUrl = "res://profile";
        }

        this.updateBody = { name: "", profile: { image: { resize_url: "" }, bio: "" } };

        this.chapters = [];
    }

    ngOnInit(): void {

        setTimeout(() => {
            this.chaptersNew = JSON.parse(Values.readString('chapters', "[]"));
        }, 1)
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.isRendering = true;
            this.isActivityLoading = true;
            this.isQuizDetailLoading = true;
            console.log('Loaded after')
            // this.chapters = this.chaptersNew
        }, 100)
    }

    onNameTextFieldLoaded(args: any) {
        this.nameTextField = args.object;
    }

    onListViewLoaded(args: any, whichList: number) {
        setTimeout(() => {
            var listView = args.object;
            listView.items = this.chaptersNew;
            if (whichList == 0) {
                this.isActivityLoading = false;
            } else {
                console.log('Loaded before')
                this.isQuizDetailLoading = false;
            }
        }, 5)
    }

    onQuizListViewLoaded(args: any) {
        setTimeout(() => {
            var listView = args.object;
            listView.items = this.chaptersNew;
            console.log('Loaded before')
            this.isQuizDetailLoading = false;
        }, 5)
    }

    onPageLoaded() {

    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onLabelLoaded(args: any) {
    }

    onCardLoaded(args: any) {
        let card = args.object;

        setTimeout(() => {
            if (card.android) {
                let nativeCardView = card.android;
                console.trace("Label:::", nativeCardView)
                nativeCardView.setRadius(20.00);
            } else if (card.ios) {
                let nativeGridMain = card.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 15)

    }

    onMenuClick(menuButton) {
        Menu.popup({
            view: menuButton,
            actions: [ { id: "two", title: this.language.setquizreminder }, { id: "three", title: this.language.changePassword }, { id: "four", title: this.language.logout }, { id: "five", title: this.language.changeLanguage }]
        })
            .then(action => {
                if (action.id == "one") {
                    alert("Clicked on Settings");
                }
                else if (action.id == "two") {
                    var body = {
                        days: [0, 6],
                        time: {
                            hours: 2,
                            minutes: 56,
                            dimension: 'am'
                        }
                    }
                    this.reminderService.showReminder();
                    // this.reminderDialog.show();
                    //hit api
                }
                else if (action.id == "three") {
                    setTimeout(() => {
                        this.isLoading = true;
                        this.routerExtensions.navigate(['/changePassword'], { clearHistory: true });
                    }, 10)
                }
                else if (action.id == "four") {
                    setTimeout(() => {
                        Values.writeString(Values.USER, "");
                        Values.writeString(Values.X_ACCESS_TOKEN, "");
                        setTimeout(() => {
                            this.isLoading = true;
                            this.routerExtensions.navigate(['/login'], { clearHistory: true });
                        }, 10)
                    }, 10)
                }
                else if (action.id == "five") {

                    setTimeout(() => {
                        this.languageSelectDialog.show();

                    }, 10)
                }
                else {
                }
            })
            .catch(console.log);
    }

    onNameTextChange(args: any) {
        var nameTextField = <TextField>args.object;
        this.nameText = nameTextField.text;
    }

    onStatusTextChange(args: any) {
        var statusTextField = <TextField>args.object;
        this.statusText = statusTextField.text;
    }

    onNameFocus(args: TextField) {
        this.nameIsFocussed = true;
        if (this.showUpdateButtons) {
            this.nameBorderColor = "#ffffff"
        }
        if (isAndroid) {
            args.android.setSelection(args.text.length)
        }
    }

    onNameBlur(args) {
        this.nameIsFocussed = false;
        this.nameBorderColor = "transparent"
    }

    onStatusFocus(args: any) {
        this.statusIsFocussed = true;
        if (this.showUpdateButtons) {
            this.statusBorderColor = "#ffffff"
        }
        if (isAndroid) {
            args.android.setSelection(args.text.length)
        }
    }

    onStatusBlur(args) {
        this.statusBorderColor = "transparent"
        this.statusIsFocussed = false;
    }

    onEditTap() {
        // var nameTextField = this.nameTextFieldRef.nativeElement as TextField;
        // // var nameTextField = <TextField>args.object;
        // console.log('Edit:::', nameTextField)
        if (this.nameTextField) {
            this.nameTextField.focus();
            this.changeDetector.detectChanges();
        }
        this.showUpdateButtons = true;
    }

    onImageEditTap() {
        console.log("ImageTapped");
        if (this.showUpdateButtons) {
            this.context
                .authorize()
                .then(() => {
                    return this.context.present();
                })
                .then((selection) => {
                    if (selection[0] != null && selection[0] != undefined) {
                        var imgAssest: ImageAsset = selection[0];
                        this.imageUrl = imgAssest.android;
                        this.uploadImage();
                        console.log("Path:::", this.imageUrl);
                    }
                }).catch(function (e) {
                    // process error
                });
        }
    }

    onDiscardTap() {
        if (this.user && this.user.name) {
            this.nameText = this.user.name;
        } else {
            this.nameText = "";
        }
        if (this.user && this.user.profile.bio) {
            this.statusText = this.user.profile.bio;
        } else {
            this.statusText = "";
        }
        if (this.user && this.user.profile && this.user.profile.image && this.user.profile.image.resize_url) {
            this.imageUrl = this.user.profile.image.resize_url;
        } else {
            this.imageUrl = "res://profile";
        }
        this.showUpdateButtons = false;
    }

    uploadImage() {

        this.isLoading = true;

        if (this.imageUrl != "res://profile") {

            var imageName = this.imageUrl.substr(this.imageUrl.lastIndexOf("/") + 1);
            var extension = imageName.substr(imageName.lastIndexOf(".") + 1);

            var mimeType = "image/" + extension;
            var uploadSession = session('image-upload');
            var request = {
                url: Values.BASE_URL + "files",
                method: "POST",
                headers: {
                    "Content-Type": "application/octet-stream",
                    "File-Name": "ProfileImage",
                    "x-access-token": Values.readString(Values.X_ACCESS_TOKEN, "")
                },
                // headers: headers,
                description: `{'uploading':${imageName}}`
            }
            const params = [
                { name: "file", filename: this.imageUrl, mimeType: mimeType },
            ]
            var task = uploadSession.multipartUpload(params, request);
            task.on("progress", (e) => {
            });

            task.on("responded", (e: ResultEventData) => {
                this.imageUrl = JSON.parse(e.data).data.image.resize_url;
                setTimeout(() => {
                    this.isLoading = false;
                    this.changeDetector.detectChanges();
                }, 5)
            });
            task.on("error", (e) => {
                console.log("ERRORRR:::::", e.error);
                alert("May be your network connection is low.");
            });
        }
    }


    onUpdateTap() {

        var headers = {
            'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ""),
            'Content-Type': 'application/json'
        }
        this.updateBody.profile.image.resize_url = "";

        if (this.imageUrl != "res://profile") {
            this.updateBody.profile.image.resize_url = this.imageUrl;
        }

        this.updateBody.name = this.nameText;
        this.updateBody.profile.bio = this.statusText;

        console.log('Body:::', this.updateBody)

        this.http.put(Values.BASE_URL + `users/${this.user._id}`, this.updateBody, { headers: headers }).subscribe((res: any) => {
            if (res && res.isSuccess) {
                console.log("RES:::", res)
                this.showUpdateButtons = false;
                Values.writeString(Values.USER, JSON.stringify(res.data))
            }
        }, (error) => {
            setTimeout(() => {
                this.isRendering = true
            }, 10);
            alert('Error: Could not get connect with server')
            console.log('Home:::Error:::', error)
            // this.errorDialog.show();
            if (error.error.statusCode == 401 && error.error.error == "unauthorized_user") {
                this.router.navigate(['/login'], {
                    clearHistory: true
                });
            }
            this.showUpdateButtons = true;
        })
    }

    changeLanguage(event: any) {
        console.log('changee::', event);
        this.languageSelectDialog.hide();
        this.languageService.setLanguage(event.value);
    }



    onLenguageSelectLoaded(args: any) {


        var dialog = <any>args.object;

        setTimeout(() => {
            if (dialog.android) {
                let nativeGridMain = dialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
            } else if (dialog.ios) {
                let nativeGridMain = dialog.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 50)

    }

}
