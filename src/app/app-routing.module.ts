import { AppComponent } from './app.component';
import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes, PreloadAllModules } from "@angular/router";
import { AuthGuard } from "./services/guards/auth.guard.service";
import { PaymentComponent } from "./payment/payment.component";

const routes: Routes = [
    // { path: "", loadChildren: "./app.module#AppModule" },
    { path: "", component:AppComponent },
    { path: "payment", component: PaymentComponent },
    { path: "login", loadChildren: "./login/login.module#LoginModule" },
    { path: "register", loadChildren: "./register/register.module#RegisterModule" },
    { path: "forgotPassword", loadChildren: "./forgot-password/forgot-password.module#ForgotPasswordModule" },
    { path: "changePassword", loadChildren: "./change-password/change-password.module#ChangePasswordModule" },
    { path: "confirmOtp", loadChildren: "./confirm-otp/confirm-otp.module#ConfirmOtpModule" },
    { path: "search", loadChildren: "./search/search.module#SearchModule" },
    { path: "home", loadChildren: "./home/home.module#HomeModule" },
    { path: "chapter", loadChildren: "./chapter/chapter.module#ChapterModule" },
    { path: "bookmark", loadChildren: "./bookmark/bookmark.module#BookmarkModule" },
    { path: "account", loadChildren: "./account/account.module#AccountModule" },
    { path: "quiz", loadChildren: "./quiz/quiz.module#QuizModule" },
    { path: "quizResult", loadChildren: "./quiz-result/quiz-result.module#QuizResultModule" }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(
        <any>routes, { preloadingStrategy: PreloadAllModules })],
    exports: [NativeScriptRouterModule]
})


export class AppRoutingModule { }
