import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NgModalModule } from "../modals/ng-modal";
import { HttpClientModule } from "@angular/common/http";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./components/home.component";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { NativeScriptBottomNavigationBarModule } from "nativescript-bottom-navigation/angular";
import { DropDownModule } from "nativescript-drop-down/angular";

@NgModule({
    bootstrap: [
        HomeComponent
    ],
    imports: [
        NativeScriptUIListViewModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NgModalModule,
        HttpClientModule,
        HomeRoutingModule,
        NativeScriptBottomNavigationBarModule,
        
        // DropDownModule
    ],
    declarations: [
        HomeComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})


export class HomeModule { }
