import { Component, OnInit, AfterContentInit, ChangeDetectorRef, NgZone, ViewChild, ElementRef } from "@angular/core";
import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { UserService } from "../../services/user.service";
import { Color } from "tns-core-modules/color/color";
import { Menu } from "nativescript-menu";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { BottomNavigationTab, TabPressedEventData, TabSelectedEventData } from "nativescript-bottom-navigation"
import { ReminderService } from "~/app/services/reminder.service";
import { LanguageService } from "../../services/language/env.service";
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { ModalComponent } from "~/app/modals/modal.component";
import { ValueList } from "nativescript-drop-down";

declare const android: any;
declare const CGSizeMake: any;
const globalAny: any = global;

class DataItem {
    heading: string;
    name: string;
    constructor(heading: string, name: string) {
        this.heading = heading;
        this.name = name;
    }
}

@Component({
    selector: "ns-home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterContentInit {
    // @ViewChild("dd", { static: false }) dropDown: ElementRef;
    @ViewChild('languageSelectDialog', { static: false }) languageSelectDialog: ModalComponent;

    bottomNatigationTabs;
    leftTopics = [];
    rightTopics = [];
    userName: string;
    public progressValue: number;
    selectedCard: number;
    selectedRightCard: number;
    chapters;
    language: any;
    selectedDays;
    langaugeIndex: number;
    languageList: any;
    isRendering: boolean;
    renderingTimeout: boolean;
    isLanguageSelect: boolean;
    isLoading: boolean;
    loadingTimeout;
    goodMorning: string;
    paymentStatus: string;

    constructor(private zone: NgZone, private languageService: LanguageService, private changeDetectorRef: ChangeDetectorRef, private routerExtensions: RouterExtensions, private userService: UserService, private reminderService: ReminderService, private http: HttpClient) {

        this.languageService.languageState.subscribe((res) => {
            if (res) {
                this.language = res;

                setTimeout(() => {
                    this.routerExtensions.navigate(['']);

                }, 100);
            }
        }, error => {
            console.log("Error related to language service:", error)
        })
        this.language = this.languageService.langauge;
        this.isRendering = false;
        this.isLanguageSelect = false;

        this.isLoading = false;
        this.goodMorning = this.language.goodMorning;
        this.paymentStatus = this.language.paymentPaid;
        this.bottomNatigationTabs = new Array<BottomNavigationTab>();

        this.chapters = [];
        this.selectedCard = -1;
        this.selectedRightCard = -1;

        this.selectedDays = [false, false, false, false, false, false, false];

        this.userName = "Saulius.";
        this.userService.activeScreen('home');
        this.userService.playButtonState(false);
        this.userService.showLoadingState(false);
        this.userService.showFooter(true);



        this.userName = JSON.parse(Values.readString(Values.USER, "")).name;


        this.getChapters();
    }

    ngOnInit(): void {
        this.languageList = this.languageService.langaugeList


        setTimeout(() => {
            this.isRendering = true;
        }, 300)


        setTimeout(() => {
            if (!Values.doesExist(Values.IS_NEW_USER)) {
                console.log("ISNEW::", Values.doesExist(Values.IS_NEW_USER))
                this.reminderService.showReminder();
                Values.writeString(Values.IS_NEW_USER, "IS_NOT_NEW");
            }
        }, 2000);
    }

    ngAfterContentInit(): void {
    }

    onBottomNavigationTabSelected(args: TabSelectedEventData) {
        console.log('Selected:::', args);
    }

    onBottomNavigationTabPressed(args: TabPressedEventData) {
        console.log('Pressed:::', args)
    }

    onBottomButtonTap(index: number) {
        switch (index) {
            case 0:
                console.log('Pressed:', index);
                break;
            case 1:
                console.log('Pressed:', index);
                break;
            case 2:
                console.log('Pressed:', index);
                break;
            case 3:
                console.log('Pressed:', index);
                break;
            case 4:
                console.log('Pressed:', index);
                break;
        }
    }

    setBottomNavigationTabs() {
        var bottomNatigationTabs = [];
        bottomNatigationTabs.push(new BottomNavigationTab({ title: 'Search', icon: 'res://search_grey', isSelectable: false }))
        bottomNatigationTabs.push(new BottomNavigationTab({ title: 'Quiz', icon: 'res://quiz_grey', isSelectable: false }))
        bottomNatigationTabs.push(new BottomNavigationTab({ title: 'Topics', icon: 'res:/topic_grey', isSelectable: true }))
        bottomNatigationTabs.push(new BottomNavigationTab({ title: 'Bookmark', icon: 'res://bookmark_grey', isSelectable: false }))
        bottomNatigationTabs.push(new BottomNavigationTab({ title: 'Account', icon: 'res://account_grey', isSelectable: false }))
    }

    getChapters() {

        var headers = {
            'x-access-token': Values.readString(Values.X_ACCESS_TOKEN, ""),
            'Content-Type': 'application/json'
        }

        this.http.get(Values.BASE_URL + `chapters`, { headers: headers }).subscribe((res: any) => {
            if (res && res.isSuccess) {
                console.log('Home:::Response:::', res)
                for (var i = 0; i < res.data.length; i++) {
                    this.chapters.push(res.data[i]);
                }
                if (this.chapters && this.chapters.length != 0) {
                    Values.writeString(Values.CHAPTERS, JSON.stringify(this.chapters))
                }
                // Values.CHAPTER = this.chapters[0];
                setTimeout(() => {
                    this.isRendering = true
                }, 10);
            }
        }, (error) => {
            setTimeout(() => {
                this.isRendering = true
            }, 10);
            alert('Error: Could not get chapters from server')
            console.log('Home:::Error:::', error)
            if (error.error.statusCode == 401 && error.error.error == "unauthorized_user") {
                this.routerExtensions.navigate(['/login'], {
                    clearHistory: true
                });
            }
            // this.errorDialog.show();
        })
    }

    onMenuClick(menuButton) {
        Menu.popup({
            view: menuButton,
            actions: [{ id: "two", title: this.language.setquizreminder }, { id: "three", title: this.language.changePassword }, { id: "four", title: this.language.logout }, { id: "five", title: this.language.changeLanguage }]
        })
            .then(action => {
                if (action.id == "one") {
                    alert("Clicked on Settings");


                }
                else if (action.id == "two") {
                    var body = {
                        days: [0, 6],
                        time: {
                            hours: 2,
                            minutes: 56,
                            dimension: 'am'
                        }
                    }
                    this.reminderService.showReminder();
                    // this.reminderDialog.show();
                    //hit api
                }
                else if (action.id == "three") {
                    setTimeout(() => {
                        this.isLoading = true;
                        this.routerExtensions.navigate(['/changePassword'], { clearHistory: true });
                    }, 10)
                }
                else if (action.id == "four") {
                    setTimeout(() => {
                        Values.writeString(Values.USER, "");
                        Values.writeString(Values.X_ACCESS_TOKEN, "");
                        setTimeout(() => {
                            this.isLoading = true;
                            this.routerExtensions.navigate(['/login'], { clearHistory: true });
                        }, 10)
                    }, 10)
                }
                else if (action.id == "five") {

                    setTimeout(() => {
                        this.languageSelectDialog.show();

                    }, 10)


                }
                else {

                }
            })
            .catch(console.log);
    }


    onDayTap(index: number) {
        switch (index) {
            case 0:
                this.selectedDays[0] = !this.selectedDays[0];
                break;
            case 1:
                this.selectedDays[1] = !this.selectedDays[1];
                break;
            case 2:
                this.selectedDays[2] = !this.selectedDays[2];
                break;
            case 3:
                this.selectedDays[3] = !this.selectedDays[3];
                break;
            case 4:
                this.selectedDays[4] = !this.selectedDays[4];
                break;
            case 5:
                this.selectedDays[5] = !this.selectedDays[5];
                break;
            case 6:
                this.selectedDays[6] = !this.selectedDays[6];
                break;
        }
    }

    onReadSample() {

    }

    cardClicked(index: number) {
        this.selectedCard = index;
        this.userService.showLoadingState(true);
        console.log('Selected Index:::', index)
        setTimeout(() => {
            this.userService.showLoadingState(false);
            setTimeout(() => {
                this.isLoading = true;
                Values.CHAPTER_NAME = this.chapters[index].name;
                this.routerExtensions.navigate(['/chapter'], {
                    queryParams: {
                        'chapter': JSON.stringify(this.chapters[index]),
                        'index': index,
                    }
                });
            }, 10)

        }, 50);
    }

    onCardLongPressed(index: number) {
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onBookLoaded(args: any) {
        var bookCard = <any>args.object;

        setTimeout(() => {
            if (bookCard.android) {
                let nativeGridMain = bookCard.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
            } else if (bookCard.ios) {
                let nativeGridMain = bookCard.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 50)

    }

    changeLanguage(event: any) {
        console.log('changee::', event);
        this.languageSelectDialog.hide();
        this.languageService.setLanguage(event.value);
    }



    onLenguageSelectLoaded(args: any) {


        var dialog = <any>args.object;

        setTimeout(() => {
            if (dialog.android) {
                let nativeGridMain = dialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
            } else if (dialog.ios) {
                let nativeGridMain = dialog.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 50)

    }


}