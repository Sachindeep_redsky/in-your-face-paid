exports.chapter = function () {
  return `


	
		
			
				
  INTRO
          -> Įvadas







“

  
      
          
              Dvi svarbiausios dienos tavo gyvenime yra diena, kai tu gimei, ir diena,
                  kai tu supratai, kodėl gimei.“
          
      
  



  Markas Tvenas


  

      Pagrindinis šios knygos tikslas yra padėti tau atrasti savo vietą šiame
      pasaulyje. Veidoskaitos metodikos autoriai tai vadina „rasti savo
      auksinį kelią“. Jei įdėmiai stebėjai žmones aplinkui, turėjai
      pastebėti, kad nors fiziškai ir esame panašūs, bet mūsų
      talentai ir gebėjimai yra skirtingi. Vienas stiprus fiziškai, kitas
      silpnesnis, trečias gabus matematikai, o kai ketvirtas pradeda
      pasakoti istoriją, jos klausantis realybė išnyksta.

      Žmogaus gyvenimas yra skirstomas į du etapus. Pirmasis etapas - iki 30
      metų. Tai etapas, kai tu dar mokaisi gyventi. Metodikos pradininkai
      jį dar vadina „Mamos veido“ periodu. Šiuo periodu tėvų įtaka
      („Mamos veidas“) turi didžiausią įtaką tavo vystymuisi.
      Antrasis etapas prasideda nuo 30 metų. Tai etapas, kai iškyla
      pagrindiniai klausimai apie tai, 

kas
          aš esu

      ir 
ko
          aš iš tikrųjų noriu
.
  









  
      Bendraudamas su žmonėmis nuolat pastebiu, kad daugelis profesiją pirmajame
      gyvenimo etape renkasi pagal tėvų rekomendacijas ar tuo metu
      visuomenėje vyraujančias madas. Tik antruoju savo gyvenimo etapu
      pradedama ieškoti tokios veiklos, kuri teikia malonumą. Stebėdamas
      žmones matau ir tai, kad tikimybė išdrįsti eiti savo keliu yra
      proporcingai susijusi su pasitikėjimu savimi. 
  


  
      Kuo
                  silpnesnis žmogaus pasitikėjimas savimi, tuo mažesnė tikimybė,
                  kad jis išdrįs eiti savo keliu.
  


Labai
      dažnai pasitaiko atvejų, kad žmogui atrasti save padeda įvairūs
      hobiai ar kitos mėgiamos veiklos. Pokalbių metu pastebėjau, kad
      daugelis sako, jog sudėtingiausia užduotis – hobį paversti
      pelninga veikla. Pavyzdžiui, žmogui patinka jodinėti arkliais, bet
      juk ne visi nori ir gali būti jojimo treneriu. Galiu
      papasakot ne vieną tikrą istoriją, kaip žmogus pasiekė savo
      tikslą. Viena iš įdomiausių – tai istorija apie žmogų, kuris
      visą gyvenimą norėjo būti architektu. Dėl silpnų tėvų jis
      negalėjo pasiekti savo tikslo įprastu būdu, bet ši sritis jį
      traukė visą gyvenimą. Nesąmoningai suvokdamas savo potraukį ir
      apribojimus jis pasirinko statybininko kelią. Pasikeitus santvarkai
      ir sulaukus brandos jis išdrįso pradėti savo verslą. Tas verslas
      yra specifinis. Jis yra nekilnojamo turto vystytojas, bet ne
      tradicinis, kaip mes tai suprantame. Šis vyras perka žemės
      sklypus, kurie tinka sodybai. Juose pats suprojektuoja svajonių
      sodybą, su nedidele pagalba iš šalies ją pastato, sutvarko
      aplinką ir parduoda. Pasakodamas man savo istoriją jis vis
      akcentuodavo, kad maloniausia veiklos dalis jam yra namo
      projektavimas ir statymas, todėl labai dažnai statydamas tobulina
      tai, kas buvo sugalvota iš pradžių. Per metus jis paruošia apie
      du projektus. Pasak pašnekovo, pinigų pakanka pragyvenimui ir kitam
      projektui vystyti. Štai tokia forma žmogus realizuoja kažkada
      neįgyvendintą savo kaip architekto svajonę. 
AUKSINIO
          KELIO METODIKA
Mūsų
      charakteris sudarytas iš savotiškų blokų. Vieni iš mūsų yra
      itin geri ir pirmai progai pasitaikius puola padėti kitiems.
      Pavadinkime šį charakterio komponentą „Motinos Teresės
      sindromu“. Dar kiti atsidūrę bet kokioje situacijoje pradeda
      automatiškai vadovauti kitiems. Šį charakterio bloką galima
      pavadinti „Kontrolierius“. Kai kurie žmonės darydami bet ką
      visame kame ieško gyvenimo prasmės. Šią savybę galima pavadinti
      „Dvasinio tėvelio genas“. Taip pat dažnai tenka sutikti žmonių,
      kurie į viską žvelgia kūrybiškai, jausdami meną ir grožį. Jie
      stengiasi rengtis gražiai, jiems spurda širdis apžiūrint daiktus
      interjero parduotuvėje. Juos galima pavadinti „Grožio kūrėjai“.
      Galėčiau išvardinti dar daug charakterio komponentų ir
      papasakoti, kaip jie atpažįstami, bet tai nėra šio skyriaus
      tikslas. 
Auksinio
          kelio metodika

      remiasi dominuojančių žmogaus talentų suliejimu į vieną tašką.
      Tarkime, jei kiekvienai charakterio savybei matuoti taikytumėme 10
      balų skalę, galėtume nustatyti, kokios savybės dominuoja ir kokia
      to prasmė. Pavyzdžiui, aptarkime žmogaus gerumą, arba, kaip jau
      pavadinau anksčiau, „Motinos Teresės sindromą“. Turbūt
      sutiksite, kad beveik visi žmonės yra geri, bet kiekvieno iš mūsų
      gerumas turi ribas. Ir jei matuodami 10 balų skale matome, kad
      daugelis veido bruožų rodo apie gebėjimą teikti pagalbą, prieš
      mus stovi žmogus, kuris turėtų save realizuoti padėdamas kitiems.
      Jam derėtų rinktis gydytojo, darželio auklėtojo, policininko ir
      panašias profesijas. Tačiau ar žmogus, turintis „Motinos
      Teresės sindromą“, taps policininku, priklausys ir nuo papildomų
      komponentų. Policininko atveju dar reikalingas „Kario“
      paramentas, kuris ir lemia, jog šio žmogaus gyvenimo misija yra
      POZYTYVIOJI AGRESIJA. Iš karto noriu pasakyti, kad
      veidoskaitos metodikos esmė pakankamai paprasta. Išsiaiškinus 3
      dominuojančius žmogaus charakterio komponentus galima pakankamai
      tiksliai nusakyti žmogaus gyvenimo misiją. Žodis „misija“
      minimas sąmoningai. Daugeliu atveju aptariant žmogaus gebėjimus
      nerekomenduojama įvardinti konkrečios profesijos, nes tai apriboja
      renkantis. Variacijų, kaip galima realizuoti savo misiją, yra
      pakankamai daug. Tarkime, jau aptarto policininko pavyzdžiu
      akivaizdu, jog jo misija yra pozityvioji agresija, tačiau realizuoti
      ją galima įvairiai: ir tarnaujant kariuomenėje, ir tampant Rytų
      kovos menų treneriui ar kalėjimo prižiūrėtoju. Ir tai dar ne
      visi variantai. Tikiuosi, esmę supratai. Sėkmės kelyje į
      veidoskaitos pasaulį, kuris padės tau geriau suprasti save!
  


  SAUGUMO
          TECHNIKA

Prie
š
      pasineriant į veidoskaitos pasaulį labai svarbu susitarti dėl
      kelių bazinių dalykų, kurie bus savotiška saugumo technika ir
      apsaugos tave nuo klaidų, kurios gali trukdyti interpretuoti žinias.
      Visų pirma, tai kūno konstitucijos reikšmė taikant
      metodiką. Pradėję įdėmiau analizuoti žmonių veidus
      pastebėsite, kad beveik kiekvieno veido parametrai skiriasi lyginant
      juos su kitais veidais. Jei viename veide nosies parametras gali būti
      drąsiai įvardintas kaip ilga nosis, tai žvelgiant į kitą veidą
      tokio paties ilgio nosis bus įvardinta kaip trumpa. Ir tai normalu.
      Kitaip tariant, vertinant bet kokį veido parametrą reikėtų
      atsižvelgti į bendrą žmogaus kūno konstituciją. Pavyzdžiui,
      jei žmogaus ūgis 1.60 m., atitinkamai skirsis ir jo galvos dydis,
      ir veido parametrai jame. Apibendrinant viską galima teigti, jog jei
      patalpoje turime kelis žmones, kurių veidai yra tiriami, nebūtinai
      laimės tas, kurio parametras yra didžiausias matuojant jį
      centimetrais. Kita svarbi detalė – auklėjimas. Kaip
      sužinosite kituose šios knygos skyriuose, veido dalys informuoja
      apie tam tikrus charakterio parametrus, bet jie visada turi pliusinę
      (teigiamą) ir minusinę (neigiamą) charakterio pusę. Nesvarbu, ką
      indikuoja konkretus veido parametras, bet ar jis yra pliusinis, ar
      minusinis, priklausys nuo aplinkos, kurioje augo žmogus.
      Lengviausias būdas tai išsiaiškinti yra nesudėtingas pokalbis su
      žmogumi neutraliomis temomis. Jei žinosite, ko ieškote,
      charakteris greitai atsiskleis visomis spalvomis. Kai jau turėsite
      pakankamai praktikos, pradėsite atskirti dominuojantį parametrą
      vien žvelgdami į veidą. Pateiksiu pavyzdį. Policininko ir
      nusikaltėlio veido parametrų kombinacija panaši. Abiejų
      savirealizacijos pagrindas yra agresija. Skirtumas tik toks, kad
      policininkas realizuoja save pozityvia agresija, o nusikaltėlis šiuo
      atveju atranda save išreikšdamas minusinį parametro variantą. Vis
      dėlto abiem atvejais pagrindas yra vienodas. Kitas svarbus
      uždavinys - interpretacija. Išmokę atskirų veido dalių
      charakterio indikacijas galite lengvai suklysti apibūdindami žmogų
      remdamiesi vos vienu bruožu. Noriu ypač pabrėžti tai, kad
      kiekvienas žmogus yra unikalus. Žinoma, yra daug panašių veidų,
      bet patikėkite manimi – veide kiekvienas milimetras gali stipriai
      pakeisti paveikslą. Iš karto galiu perspėti, kad pagrindinę
      spalvą charakteriui suteikia 3 labiausiai dominuojantys veido
      bruožai. Kiti bruožai bus lyg atspalviai, bet spalvos ir atspalvių
      kombinacija ir sukuria tą įvairovę, kurią matome žvelgdami į
      žmogų. 






Na
      ir paskutinis dalykas, kurį verta paminėti, yra tai, kad kiekvienas
      veidas yra tobulas. Taip, jums nepasigirdo – tobulas! Žmonės,
      kurie turi jautresnį charakterį, kartais turi savybę labiau
      akcentuoti minusinį parametrą ir net savyje ieškoti daugiau minusų
      nei teigiamų dalykų. Noriu, kad visą kelionės laiką prisimintum:
      
Toks,
          koks tu esi, esi tobulas! Jei esi čia, reiškia, tai yra prasminga!

      Sėkmės veidų skaitymo pasaulyje! Perskaitęs šią knygą
      jau niekada į žmonių veidus nebežiūrėsi taip, kaip iki šiol.
      Nuo šiol jie tau bus neišsemiami informacijos lobynai. Manęs
      dažnai klausia, ar manęs neerzina gebėjimas skaityti žmonių
      veidus? Į šį klausimą atsakau paprastai: įsivaizduokite, kad
      esate svetimoje šalyje ir suprantate tos šalies gyventojų kalbą...
      Ar tai ne puiku?





`
}