import { ReminderService } from '~/app/services/reminder.service';
import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { Values } from './../../values/values';
import { Component, OnInit, AfterContentInit, ViewChild } from "@angular/core";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { UserService } from "../../services/user.service";
import { Menu } from "nativescript-menu";
import { Router } from "@angular/router";
import { ModalComponent } from "~/app/modals/modal.component";
import { TimePicker } from "tns-core-modules/ui/time-picker/time-picker";
import { Page, Color } from "tns-core-modules/ui/page/page";
import { Label } from 'tns-core-modules/ui/label';
import { AnimationCurve } from 'tns-core-modules/ui/enums/enums';
import * as LNG from "../../services/language/env.service";
import { LanguageService } from '../../services/language/env.service';

declare var android: any;
declare const CGSizeMake: any;

@Component({
    selector: "ns-search",
    moduleId: module.id,
    templateUrl: "./search.component.html",
    styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, AfterContentInit {
    @ViewChild('reminderDialog', { static: false }) reminderDialog: ModalComponent;
    @ViewChild('languageSelectDialog', { static: false }) languageSelectDialog: ModalComponent;

    language: any;
    isRendering: boolean;
    renderingTimeout;

    isLoading: boolean;
    loadingTimeout;
    selectedDays;
    languageList: any;
    searchedItems;

    ch1: string;

    str: string;

    intro;
    neck;
    jaw;
    chin;
    nose;
    ears;
    lips;
    eyes;

    constructor(private languageService: LanguageService, private userService: UserService, private routerExtensions: RouterExtensions, private router: Router, private page: Page, private reminderService: ReminderService) {
        this.languageService.languageState.subscribe((res) => {
            if (res) {
                this.language = res;
            }
        }, error => {
            console.log("Error related to language service:", error)
        })
        this.language = this.languageService.langauge;

        this.isRendering = false;
        this.isLoading = false;
        this.languageList = this.languageService.langaugeList

        this.userService.activeScreen('search');
        this.userService.playButtonState(false);
        this.userService.showLoadingState(false);
        this.userService.showFooter(true);

        this.searchedItems = [];
        this.str = require('./intro').chapter();
        this.intro = [];
        this.neck = [];
        this.jaw = [];
        this.chin = [];
        this.nose = [];
        this.ears = [];
        this.lips = [];
        this.eyes = [];

    }

    ngOnInit(): void {
        this.renderingTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 300)
    }

    ngAfterContentInit(): void {
        // this.renderingTimeout = setTimeout(() => {
        //     this.isRendering = true;
        // }, 50)
    }


    onMenuClick(menuButton) {
        Menu.popup({
            view: menuButton,
            actions: [ { id: "two", title: this.language.setquizreminder }, { id: "three", title: this.language.changePassword }, { id: "four", title: this.language.logout }, { id: "five", title: this.language.changeLanguage }]
        })
            .then(action => {
                if (action.id == "one") {
                    alert("Clicked on Settings");


                }
                else if (action.id == "two") {
                    var body = {
                        days: [0, 6],
                        time: {
                            hours: 2,
                            minutes: 56,
                            dimension: 'am'
                        }
                    }
                    this.reminderService.showReminder();
                    // this.reminderDialog.show();
                    //hit api
                }
                else if (action.id == "three") {
                    setTimeout(() => {
                        this.isLoading = true;
                        this.routerExtensions.navigate(['/changePassword'], { clearHistory: true });
                    }, 10)
                }
                else if (action.id == "four") {
                    setTimeout(() => {
                        Values.writeString(Values.USER, "");
                        Values.writeString(Values.X_ACCESS_TOKEN, "");
                        setTimeout(() => {
                            this.isLoading = true;
                            this.routerExtensions.navigate(['/login'], { clearHistory: true });
                        }, 10)
                    }, 10)
                }
                else if (action.id == "five") {

                    setTimeout(() => {
                        this.languageSelectDialog.show();

                    }, 10)


                }
                else {

                }
            })
            .catch(console.log);
    }

    //Update to new values
    onReminderDone() {
        this.reminderDialog.hide();
    }

    //Reset to the previous values
    onReminderCancel() {
        this.reminderDialog.hide();
    }

    onDayTap(index: number) {
        switch (index) {
            case 0:
                this.selectedDays[0] = !this.selectedDays[0];
                break;
            case 1:
                this.selectedDays[1] = !this.selectedDays[1];
                break;
            case 2:
                this.selectedDays[2] = !this.selectedDays[2];
                break;
            case 3:
                this.selectedDays[3] = !this.selectedDays[3];
                break;
            case 4:
                this.selectedDays[4] = !this.selectedDays[4];
                break;
            case 5:
                this.selectedDays[5] = !this.selectedDays[5];
                break;
            case 6:
                this.selectedDays[6] = !this.selectedDays[6];
                break;
        }
    }

    onPickerLoaded(args) {
        let timePicker = <TimePicker>args.object;

        timePicker.hour = 9;
        timePicker.minute = 25;
    }

    onTimeChanged(args) {
        console.log(args.value);
    }

    getIndicesOf(searchStr, str, caseSensitive) {
        var searchStrLen = searchStr.length;
        if (searchStrLen == 0) {
            return [];
        }
        var startIndex = 0, index, indices = [];
        // if (!caseSensitive) {
        //     str = str.toLowerCase();
        //     searchStr = searchStr.toLowerCase();
        // }
        while ((index = str.indexOf(searchStr, startIndex)) > -1) {
            indices.push(index);
            startIndex = index + searchStrLen;
        }
        return indices;
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onSearchChapterLoaded(args: any) {
        let searchChapter = args.object;



        setTimeout(() => {
            if (searchChapter.android) {
                let nativeGridMain = searchChapter.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
                nativeGridMain.set
            } else if (searchChapter.ios) {
                let nativeGridMain = searchChapter.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 20)
    }

    onTextChanged(args) {

        // console.log('indeces:::', indices)

        // for (var i = 0; i < indices.length; i++) {
        //     this.searchedItems = { "searchedLine": this.ch1.substring(indices[i], indices[i] + 30), "nthOccurrance": i }
        // }


        // var regEx = new RegExp(searchValue, 'gi')

        // var resultArray = this.ch1.match(regEx)

        // console.log('RES:::', resultArray);

        // this.myItems = new ObservableArray<Data>();
        // if (searchValue !== "") {
        //     for (let i = 0; i < this.items.length; i++) {
        //         if (this.items[i].name.toLowerCase().indexOf(searchValue) !== -1) {
        //             this.myItems.push(this.items[i]);
        //         }
        //     }
        // }
        // else {
        //     for (let i = 0; i < this.items.length; i++) {
        //         this.myItems.push(this.items[i]);
        //     }
        // }
    }

    onSearchReturnPress(args: any) {
        let searchValue = args.object.text;
        this.isLoading = true;
        setTimeout(() => {
            for (var i = 0; i < 8; i++) {
                switch (i) {
                    case 0:
                        var str = require('./intro').chapter();
                        var indices = this.getIndicesOf(searchValue, str, true);

                        this.intro = [];
                        var searchedItem = { "chapter": "Intro", "results": [] }
                        // this.searchedItems.push({ "chapter": "Intro", "results": str.substring(indices[i], indices[i] + 30), "nthOccurrance": i });

                        for (var j = 0; j < indices.length; j++) {
                            this.intro.push({ "searchValue": searchValue, "line": str.substring(indices[j], indices[j] + 30), "nthOccurance": j });
                        }

                        // this.searchedItems.push(searchedItem);
                        // console.log('str:::', this.searchedItems)

                        break;
                    case 1:
                        var str = require('./neck').chapter();
                        console.log('str:::', str)
                        var indices = this.getIndicesOf(searchValue, str, true);
                        // var searchedItem = { "chapter": "Neck", "results": [] }
                        // // this.searchedItems.push({ "chapter": "Intro", "results": str.substring(indices[i], indices[i] + 30), "nthOccurrance": i });

                        // for (var j = 0; j < indices.length; j++) {
                        //     searchedItem.results.push({ "line": str.substring(indices[j], indices[j] + 30), "nthOccurrance": j });
                        // }

                        // this.searchedItems.push(searchedItem);


                        this.neck = [];
                        var searchedItem = { "chapter": "Intro", "results": [] }
                        // this.searchedItems.push({ "chapter": "Intro", "results": str.substring(indices[i], indices[i] + 30), "nthOccurrance": i });

                        for (var j = 0; j < indices.length; j++) {
                            this.neck.push({ "searchValue": searchValue, "line": str.substring(indices[j], indices[j] + 30), "nthOccurance": j });
                        }

                        break;
                    case 2:
                        var str = require('./jaw').chapter();
                        var indices = this.getIndicesOf(searchValue, str, true);
                        this.jaw = [];
                        var searchedItem = { "chapter": "Intro", "results": [] }
                        // this.searchedItems.push({ "chapter": "Intro", "results": str.substring(indices[i], indices[i] + 30), "nthOccurrance": i });

                        for (var j = 0; j < indices.length; j++) {
                            this.jaw.push({ "searchValue": searchValue, "line": str.substring(indices[j], indices[j] + 30), "nthOccurance": j });
                        }
                        break;
                    case 3:
                        var str = require('./chin').chapter();
                        var indices = this.getIndicesOf(searchValue, str, true);
                        this.chin = [];
                        var searchedItem = { "chapter": "Intro", "results": [] }
                        // this.searchedItems.push({ "chapter": "Intro", "results": str.substring(indices[i], indices[i] + 30), "nthOccurrance": i });

                        for (var j = 0; j < indices.length; j++) {
                            this.chin.push({ "searchValue": searchValue, "line": str.substring(indices[j], indices[j] + 30), "nthOccurance": j });
                        }
                        break;
                    case 4:
                        var str = require('./nose').chapter();
                        var indices = this.getIndicesOf(searchValue, str, true);
                        this.nose = [];
                        var searchedItem = { "chapter": "Intro", "results": [] }
                        // this.searchedItems.push({ "chapter": "Intro", "results": str.substring(indices[i], indices[i] + 30), "nthOccurrance": i });

                        for (var j = 0; j < indices.length; j++) {
                            this.nose.push({ "searchValue": searchValue, "line": str.substring(indices[j], indices[j] + 30), "nthOccurance": j });
                        }
                        break;
                    case 5:
                        var str = require('./ears').chapter();
                        var indices = this.getIndicesOf(searchValue, str, true);
                        this.ears = [];
                        var searchedItem = { "chapter": "Intro", "results": [] }
                        // this.searchedItems.push({ "chapter": "Intro", "results": str.substring(indices[i], indices[i] + 30), "nthOccurrance": i });

                        for (var j = 0; j < indices.length; j++) {
                            this.ears.push({ "searchValue": searchValue, "line": str.substring(indices[j], indices[j] + 30), "nthOccurance": j });
                        }
                        break;
                    case 6:
                        var str = require('./lips').chapter();
                        var indices = this.getIndicesOf(searchValue, str, true);
                        this.lips = [];
                        var searchedItem = { "chapter": "Intro", "results": [] }
                        // this.searchedItems.push({ "chapter": "Intro", "results": str.substring(indices[i], indices[i] + 30), "nthOccurrance": i });

                        for (var j = 0; j < indices.length; j++) {
                            this.lips.push({ "searchValue": searchValue, "line": str.substring(indices[j], indices[j] + 30), "nthOccurance": j });
                        }
                        break;
                    case 7:
                        var str = require('./eyes').chapter();
                        var indices = this.getIndicesOf(searchValue, str, true);
                        this.eyes = [];
                        var searchedItem = { "chapter": "Intro", "results": [] }
                        // this.searchedItems.push({ "chapter": "Intro", "results": str.substring(indices[i], indices[i] + 30), "nthOccurrance": i });

                        for (var j = 0; j < indices.length; j++) {
                            this.eyes.push({ "searchValue": searchValue, "line": str.substring(indices[j], indices[j] + 30), "nthOccurance": j });
                        }
                        break;
                }

            }
            setTimeout(() => {
                this.isLoading = false;
            }, 10)
        }, 100)

    }

    onLabelResultClicked(chapter: string, result: any, args: any) {
        console.log('tapped')
        var label = args.object as Label;

        var chapterObj;

        // var p: string;
        // p.toLowerCase()
        var chapters = JSON.parse(Values.readString('chapters', '[]'))

        console.log('Chap:::', chapters)

        for (var i = 0; i < chapters.length; i++) {
            if (chapter == chapters[i].name.toLowerCase()) {
                chapterObj = chapters[i];
            }
        }

        console.log('Chap:::', chapterObj)

        if (chapterObj == undefined) {
            return;
        }


        label.animate({
            backgroundColor: new Color("#888888"),
            duration: 50,
            opacity: 0.5,
            curve: AnimationCurve.easeInOut
        }).then(() => label.animate({
            backgroundColor: new Color("#ffffff"),
            duration: 50,
            opacity: 1,
            curve: AnimationCurve.easeInOut
        }));

        setTimeout(() => {
            this.isLoading = true;
            this.routerExtensions.navigate(['/chapter'], {
                queryParams: {
                    search: JSON.stringify({
                        chapter: chapterObj,
                        text: result.searchValue,
                        nthOccurance: Number.parseInt(result.nthOccurance)    //1-indexed is required on server
                    })
                }
            })
        }, 10)




        // const circle = new Label();

        // const btnHeight = Number(btn.height);
        // const btnWidth = Number(btn.width);

        // const d = Math.max(btnHeight, btnWidth);

        // circle.width = d;
        // circle.height = d;
        // circle.borderRadius = d / 2;
        // circle.top = y - d / 2;
        // circle.left = x - d / 2;
        // circle.backgroundColor = 'white';
        // circle.opacity = 0;

        // wrapper.addChild(circle);

        // circle.animate({
        //     scale: { x: 0, y: 0 },
        //     opacity: 0.4,
        //     duration: 1
        // }).then(() => {
        //     circle.animate({
        //         scale: { x: 2, y: 2 },
        //         opacity: 0,
        //         duration: 500
        //     }).then(() => {
        //         wrapper.removeChild(circle);
        //     });
        // });
    }

    onFocus(args) {

    }

    onBlur(args) {

    }

    changeLanguage(event: any) {
        console.log('changee::', event);
        this.languageSelectDialog.hide();
        this.languageService.setLanguage(event.value);
    }



    onLenguageSelectLoaded(args: any) {


        var dialog = <any>args.object;

        setTimeout(() => {
            if (dialog.android) {
                let nativeGridMain = dialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
            } else if (dialog.ios) {
                let nativeGridMain = dialog.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 50)

    }

}
