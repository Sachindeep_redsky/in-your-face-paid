import * as appSettings from "application-settings"

export class Values {

    public static X_ACCESS_TOKEN: string = "x-access-token"
    public static BASE_URL: string = "http://3.122.223.89:7000/api/"
    public static GOOGLE_MAP_URL: string = "https://maps.googleapis.com/maps/api/geocode/json?"
    public static USER: string = "user";
    public static IS_NEW_USER: string = "isNewUser"
    public static CHAPTER_NAME: string = ""
    public static appComponentContext: any;
    public static CHAPTERS: string = "chapters"
    public static CURRENT_CHAPTER: string = "";
    public static LANGUAGE: string = "language";
    static ALARM_TIME: string = "alarmTime"
    static REMINDER: string = "reminder";

    public static writeString(key: string, value: string): void {
        appSettings.setString(key, value);
    }

    public static readString(key: string, defaultValue: string): string {
        return appSettings.getString(key, defaultValue);
    }

    public static writeNumber(key: string, value: number): void {
        appSettings.setNumber(key, value);
    }

    public static readNumber(key: string, defaultValue: number): number {
        return appSettings.getNumber(key, defaultValue);
    }

    public static writeBoolean(key: string, value: boolean): void {
        appSettings.setBoolean(key, value);
    }

    public static readBoolean(key: string, defaultValue: boolean): boolean {
        return appSettings.getBoolean(key, defaultValue);
    }

    public static doesExist(key: string): boolean {
        return appSettings.hasKey(key);
    }

    public static remove(key: string) {
        appSettings.remove(key);
    }


}