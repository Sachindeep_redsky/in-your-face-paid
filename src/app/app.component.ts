import { Values } from '~/app/values/values';
import { TNSTextToSpeech, SpeakOptions } from 'nativescript-texttospeech';
import { Progress } from 'tns-core-modules/ui/progress';
import { Component, OnInit, AfterContentInit, AfterViewInit, ViewChild, ChangeDetectorRef, NgZone } from "@angular/core";
import { RouterExtensions } from 'nativescript-angular/router/router-extensions'
import { Router, NavigationEnd } from "@angular/router";
import { UserService } from "./services/user.service";
import { registerElement } from 'nativescript-angular/element-registry';
import { CardView } from "@nstudio/nativescript-cardview";
import { AuthService } from "./services/auth.service";
import { initializeOnAngular } from 'nativescript-image-cache';
import { TimePicker } from 'tns-core-modules/ui/time-picker/time-picker';
import { ModalComponent } from './modals/modal.component';
import { Color } from 'tns-core-modules/color/color';
import { ReminderService } from './services/reminder.service';
import { PlayerService } from './services/player.service';
import * as LocalNotifications from 'nativescript-local-notifications';
import { AlarmUpdater } from './services/alarm/alarm.updater';
import { isAndroid } from 'platform';
import { LanguageService } from "./services/language/env.service";
import * as application from "tns-core-modules/application";
import * as Toast from 'nativescript-toast';
import { exit } from 'nativescript-exit';
registerElement("CardView", () => CardView as any);

declare var android: any;
declare const CGSizeMake: any;
const globalAny: any = global;

@Component({
    selector: "ns-app",
    moduleId: module.id,
    templateUrl: "./app.component.html",
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterContentInit, AfterViewInit {
    @ViewChild('reminderDialog', { static: false }) reminderDialog: ModalComponent;
    language: any;
    searchColor = "#9E9E9E";
    quizColor = "#9E9E9E";
    topicsColor = "#273BD3";
    bookmarkColor = "#9E9E9E";
    accountColor = "#9E9E9E";
    searchIcon = "res://search_grey";
    quizIcon = "res://quiz_grey";
    topicsIcon = "res://topic_blue";
    bookmarkIcon = "res://bookmark_grey";
    accountIcon = "res://account_grey";
    search: string;
    quiz: string;
    account: string;
    topics: string;
    bookmark: string;
    selectDay: string;
    selectTime: string;
    cancel: string;
    done: string;
    showPlayButton: boolean;
    showLoading: boolean;
    showFooter: boolean;
    selectedDays;
    isRendering: boolean;
    renderingTimeout;
    activeScreen: string;
    isSpeaking: boolean;
    tries: number;
    TTS: TNSTextToSpeech;
    speakOptions: SpeakOptions = {
        text: '', /// *** required ***
        speakRate: 0.5, // optional - default is 1.0
        pitch: 1.0, // optional - default is 1.0
        volume: 1.0, // optional - default is 1.0
        locale: 'en-GB', // optional - default is system locale,
    };

    hasBeenIntialized: boolean;
    readingChapter: string;
    selectedReminderDate: Date;

    constructor(private routerExtensions: RouterExtensions, private userService: UserService, private authService: AuthService, private router: Router, private reminderService: ReminderService,
        private playerService: PlayerService, private alarmUpdater: AlarmUpdater, private languageService: LanguageService, private changeDetectorRef: ChangeDetectorRef, private ngZone: NgZone) {

        initializeOnAngular();
        this.languageService.languageState.subscribe((res) => {
            if (res) {
                this.language = res;
                this.changeDetectorRef.detectChanges();
                globalAny.process.restart()
            }
        }, error => {
            console.log("Error related to language service:", error)
        })
        if (!(Values.doesExist(Values.LANGUAGE))) {
            this.languageService.setLanguage('en')
            // Values.writeString(Values.LANGUAGE, 'en');

        } else {
            if (!this.languageService.langauge) {
                this.languageService.setLanguage(Values.readString(Values.LANGUAGE, 'en'));
            }

        }
        this.playerService.playerState.subscribe((state: any) => {
            console.log('PlayerserviceRes..', state)
            this.onPlayClick();
        })
        this.language = this.languageService.langauge;
        this.selectDay = this.language.selectDay;
        this.selectTime = this.language.selectTime;
        this.done = this.language.done;
        this.cancel = this.language.cancel;
        this.isRendering = false;
        this.search = this.language.search;
        this.quiz = this.language.quizButton;
        this.account = this.language.account
        this.topics = this.language.topics;
        this.bookmark = this.language.bookmark
        this.isSpeaking = true;
        this.activeScreen = "";
        Values.appComponentContext = this;

        this.selectedDays = [false, false, false, false, false, false, false];

        if (Values.readString(Values.REMINDER, "[]")) {
            this.selectedDays = JSON.parse(Values.readString(Values.REMINDER, "[]"));
        }

        this.hasBeenIntialized = false;
        this.readingChapter = ""

        this.authService.isAuthenticated().then((isLogged: boolean) => {
            console.log('isLogged', isLogged)
            if (isLogged) {
                this.routerExtensions.navigate(['/home'])
            } else {
                this.routerExtensions.navigate(['/login'])
            }
        }, error => {
            console.log('isLogged', error)
            this.routerExtensions.navigate(['/login'])
        })

        this.reminderService.reminderState.subscribe((res: boolean) => {
            if (res) {
                this.reminderDialog.show();
            }
        })

        this.userService.playbuttonState.subscribe((state: boolean) => {
            if (state != undefined) {
                this.showPlayButton = state;
            }
        });
        this.userService.showloadingState.subscribe((state: boolean) => {
            if (state != undefined) {
                this.showLoading = state;
            }
        });
        this.userService.showquizResult.subscribe((state: boolean) => {
            if (state == true) {
                // this.show = state;
                this.routerExtensions.navigate(['/quizResult']);
            }
        });
        this.userService.showfooter.subscribe((state: boolean) => {
            if (state != undefined) {
                this.showFooter = state;
            }
        });

        this.userService.activescreen.subscribe((screen: string) => {
            console.log('currentScreen', screen);
            this.TTS.pause();
            if (screen == "search") {
                this.searchColor = "#273BD3";
                this.quizColor = "#9E9E9E";
                this.topicsColor = "#9E9E9E";
                this.bookmarkColor = "#9E9E9E";
                this.accountColor = "#9E9E9E";
                this.searchIcon = "res://search_blue";
                this.quizIcon = "res://quiz_grey";
                this.topicsIcon = "res://topic_grey";
                this.bookmarkIcon = "res://bookmark_grey";
                this.accountIcon = "res://account_grey";
            }
            if (screen == "quiz") {
                this.searchColor = "#9E9E9E";
                this.quizColor = "#273BD3";
                this.topicsColor = "#9E9E9E";
                this.bookmarkColor = "#9E9E9E";
                this.accountColor = "#9E9E9E";
                this.searchIcon = "res://search_grey";
                this.quizIcon = "res://quiz_blue";
                this.topicsIcon = "res://topic_grey";
                this.bookmarkIcon = "res://bookmark_grey";
                this.accountIcon = "res://account_grey";
            }
            if (screen == "home") {
                this.searchColor = "#9E9E9E";
                this.quizColor = "#9E9E9E";
                this.topicsColor = "#273BD3";
                this.bookmarkColor = "#9E9E9E";
                this.accountColor = "#9E9E9E";
                this.searchIcon = "res://search_grey";
                this.quizIcon = "res://quiz_grey";
                this.topicsIcon = "res://topic_blue";
                this.bookmarkIcon = "res://bookmark_grey";
                this.accountIcon = "res://account_grey";
            }
            if (screen == "bookmark") {
                this.searchColor = "#9E9E9E";
                this.quizColor = "#9E9E9E";
                this.topicsColor = "#9E9E9E";
                this.bookmarkColor = "#273BD3";
                this.accountColor = "#9E9E9E";
                this.searchIcon = "res://search_grey";
                this.quizIcon = "res://quiz_grey";
                this.topicsIcon = "res://topic_grey";
                this.bookmarkIcon = "res://bookmark_blue";
                this.accountIcon = "res://account_grey";
            }
            if (screen == "account") {
                this.searchColor = "#9E9E9E";
                this.quizColor = "#9E9E9E";
                this.topicsColor = "#9E9E9E";
                this.bookmarkColor = "#9E9E9E";
                this.accountColor = "#273BD3";
                this.searchIcon = "res://search_grey";
                this.quizIcon = "res://quiz_grey";
                this.topicsIcon = "res://topic_grey";
                this.bookmarkIcon = "res://bookmark_grey";
                this.accountIcon = "res://account_blue";
            }
        });


        this.TTS = new TNSTextToSpeech();

        this.TTS.speak(this.speakOptions).then(
            () => {
            },
            err => {
            }
        );

        this.router.events.subscribe((path: any) => {
            if (path instanceof NavigationEnd) {
                var url = this.router.url.split("?", 1)[0];
                console.log('routeUrl', url);
                this.activeScreen = url;

            }
        });
        this.tries = 0;
        if (isAndroid) {
            this.ngZone.run(() => {
                this.tries = 0;
                application.android.on(application.AndroidApplication.activityBackPressedEvent, (data: application.AndroidActivityBackPressedEventData) => {
                    // this.activeScreen = this.userService.currentPage
                    console.log("SCREEN::::", this.activeScreen);
                    if (this.activeScreen == "/home") {
                        data.cancel = (this.tries++ > 1) ? false : true;
                        if (this.tries == 1) {
                            Toast.makeText("Press again to exit", "short").show();
                        }
                        if (this.tries == 2) {
                            exit();
                        }
                        setTimeout(() => {
                            this.tries = 0;
                        }, 1000);
                    } else if (this.activeScreen == "/login") {
                        data.cancel = true;
                        // this.routerExtensions.back();
                        exit();
                    }
                    else {
                        data.cancel = true;
                        this.routerExtensions.back();
                    }
                });
            });
        }


        // var date = new Date();
        // date.setHours(0, 0, 0, 0);
        // console.log("dae:::", date.getTime()-date.get)
        // LocalNotifications.LocalNotifications.schedule([{
        //     id: 1,
        //     title: 'Scheduling reminder',
        //     body: 'This is InYourFace app scheduling your app reminder',
        //     ticker: 'The ticker',
        //     color: new Color("red"),
        //     badge: 1,
        //     ongoing: false, // makes the notification ongoing (Android only)
        //     icon: 'res://profile',
        //     image: "https://cdn-images-1.medium.com/max/1200/1*c3cQvYJrVezv_Az0CoDcbA.jpeg",
        //     thumbnail: true,
        //     interval: 'day',
        //     channel: 'IN_YOUR_FACE', // default: 'Channel'
        //     at: date
        // }]).then(
        //     function (scheduledIds) {
        //         console.log("Notification id(s) scheduled: " + JSON.stringify(scheduledIds));
        //     },
        //     function (error) {
        //         console.log("scheduling error: " + error);
        //     }
        // )

        // LocalNotifications.LocalNotifications.schedule([{
        //     id: 2, // generated id if not set
        //     title: 'The title',
        //     body: 'Recurs every minute until cancelled',
        //     ticker: 'The ticker',
        //     color: new Color("red"),
        //     badge: 1,
        //     ongoing: false, // makes the notification ongoing (Android only)
        //     icon: 'res://profile',
        //     image: "https://cdn-images-1.medium.com/max/1200/1*c3cQvYJrVezv_Az0CoDcbA.jpeg",
        //     thumbnail: true,
        //     interval: 'minute',
        //     channel: 'My Channel', // default: 'Channel'
        //     at: new Date(new Date().getTime() + (10 * 1000)) // 10 seconds from now
        // }]).then(
        //     function (scheduledIds) {
        //         console.log("Notification id(s) scheduled: " + JSON.stringify(scheduledIds));
        //     },
        //     function (error) {
        //         console.log("scheduling error: " + error);
        //     }
        // )
    }

    ngOnInit(): void {
    }

    ngAfterContentInit(): void {
        this.renderingTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 50)
    }

    ngAfterViewInit(): void {

    }

    onSearchTap() {
        this.searchColor = "#273BD3";
        this.quizColor = "#9E9E9E";
        this.topicsColor = "#9E9E9E";
        this.bookmarkColor = "#9E9E9E";
        this.accountColor = "#9E9E9E";
        this.searchIcon = "res://search_blue";
        this.quizIcon = "res://quiz_grey";
        this.topicsIcon = "res://topic_grey";
        this.bookmarkIcon = "res://bookmark_grey";
        this.accountIcon = "res://account_grey";
        this.routerExtensions.navigate(['/search']);
    }

    onQuizTap() {
        this.searchColor = "#9E9E9E";
        this.quizColor = "#273BD3";
        this.topicsColor = "#9E9E9E";
        this.bookmarkColor = "#9E9E9E";
        this.accountColor = "#9E9E9E";
        this.searchIcon = "res://search_grey";
        this.quizIcon = "res://quiz_blue";
        this.topicsIcon = "res://topic_grey";
        this.bookmarkIcon = "res://bookmark_grey";
        this.accountIcon = "res://account_grey";
        this.routerExtensions.navigate(['/quiz'], {
            clearHistory: true
        });
    }

    onTopicsTap() {
        this.searchColor = "#9E9E9E";
        this.quizColor = "#9E9E9E";
        this.topicsColor = "#273BD3";
        this.bookmarkColor = "#9E9E9E";
        this.accountColor = "#9E9E9E";
        this.searchIcon = "res://search_grey";
        this.quizIcon = "res://quiz_grey";
        this.topicsIcon = "res://topic_blue";
        this.bookmarkIcon = "res://bookmark_grey";
        this.accountIcon = "res://account_grey";
        this.routerExtensions.navigate(['/home']);
    }

    onBookmarkTap() {
        this.searchColor = "#9E9E9E";
        this.quizColor = "#9E9E9E";
        this.topicsColor = "#9E9E9E";
        this.bookmarkColor = "#273BD3";
        this.accountColor = "#9E9E9E";
        this.searchIcon = "res://search_grey";
        this.quizIcon = "res://quiz_grey";
        this.topicsIcon = "res://topic_grey";
        this.bookmarkIcon = "res://bookmark_blue";
        this.accountIcon = "res://account_grey";
        this.routerExtensions.navigate(['/bookmark']);
    }

    onAccountTap() {
        this.searchColor = "#9E9E9E";
        this.quizColor = "#9E9E9E";
        this.topicsColor = "#9E9E9E";
        this.bookmarkColor = "#9E9E9E";
        this.accountColor = "#273BD3";
        this.searchIcon = "res://search_grey";
        this.quizIcon = "res://quiz_grey";
        this.topicsIcon = "res://topic_grey";
        this.bookmarkIcon = "res://bookmark_grey";
        this.accountIcon = "res://account_blue";
        this.routerExtensions.navigate(['/account']);
    }

    onPlayClick() {
        console.log('Tapped Play')
        var requiredStringArray = [];
        var requiredString = "";

        if (this.readingChapter == Values.CHAPTER_NAME) {
            console.log('ChapterName:::', this.readingChapter)
            console.log('isPlayying:::', this.isSpeaking)
            if (this.isSpeaking) {
                this.TTS.pause();
            } else {
                this.TTS.resume();
            }
            this.isSpeaking = !this.isSpeaking;
        } else {
            // requiredString = "hello test Voice is hare"
            switch (Values.CHAPTER_NAME) {
                case 'Intro':
                    var chap = require("./search/components/intro").chapter();
                    console.log('chapterFileComiing', chap)
                    requiredString = chap;
                    break;
                case 'Neck':
                    var chap = require("./search/components/neck");
                    requiredString = chap.chapter();
                    break;
                case 'Jaw':
                    var chap = require("./search/components/jaw");
                    requiredString = chap.chapter();
                    break;
                case 'Chin':
                    var chap = require("./search/components/chin");
                    requiredString = chap.chapter();
                    break;
                case 'Nose':
                    var chap = require("./search/components/nose");
                    requiredString = chap.chapter();
                    break;
                case 'Ears':
                    var chap = require("./search/components/ears");
                    requiredString = chap.chapter();
                    break;
                case 'Lips':
                    var chap = require("./search/components/lips");
                    requiredString = chap.chapter();
                    break;
                case 'Eyes':
                    var chap = require("./search/components/eyes");
                    requiredString = chap.chapter();
                    break;
            }
            this.readingChapter = Values.CHAPTER_NAME;
            console.log('ChapterNameInit:::', this.readingChapter)

            var index;
            if (requiredString.length > 3900) {
                index = requiredString.length / 3900;
                if (index > parseInt(index)) {
                    index = index + 1;
                }
                var startIndex = 0
                var endIndex;
                for (var i = 0; i < index; i++) {
                    if (startIndex + 3900 < requiredString.length) {
                        endIndex = startIndex + 3900;
                        requiredStringArray.push(requiredString.slice(startIndex, endIndex))
                        startIndex = endIndex;
                    } else {
                        requiredStringArray.push(requiredString.slice(startIndex))
                    }
                }

                console.log("Arr:::", requiredStringArray)
                setTimeout(() => {
                    this.player(requiredStringArray, 0);
                }, 100)



                // for (var j = 0; j < requiredStringArray.length; j++) {

                //     var currIndex = 0;

                //     while (currIndex < requiredStringArray.length)
                //         this.speakOptions = {
                //             text: requiredStringArray[currIndex],
                //             speakRate: 0.5,
                //             pitch: 1.0,
                //             volume: 1.0,
                //             locale: 'en-GB',
                //             queue: true,
                //             finishedCallback: (() => {
                //                 if (currIndex + 1 <= requiredStringArray.length) {
                //                     currIndex = currIndex + 1;
                //                 }
                //                 // this.TTS.speak(this.speakOptions).then(
                //                 //     () => {
                //                 //         this.isSpeaking == true
                //                 //         console.log('PlayREs:::')
                //                 //     },
                //                 //     err => {
                //                 //         console.log('PlayRej:::', err)
                //                 //         this.isSpeaking == false
                //                 //     }
                //                 // );
                //             })
                //         };

                //     this.TTS.speak(this.speakOptions).then(
                //         () => {
                //             this.isSpeaking == true
                //             console.log('PlayREs:::')
                //         },
                //         err => {
                //             console.log('PlayRej:::', err)
                //             this.isSpeaking == false
                //         }
                //     );
                // }
            } else {

                this.speakOptions = {
                    text: requiredString,
                    speakRate: 0.5,
                    pitch: 1.0,
                    volume: 1.0,
                    locale: 'en-GB',
                    queue: false,
                    // finishedCallback: (() => {
                    //     if (currIndex + 1 <= requiredStringArray.length) {
                    //         currIndex = currIndex + 1;
                    //     }
                    // })
                };

                this.TTS.speak(this.speakOptions).then(
                    () => {
                        this.isSpeaking == true
                        console.log('PlayREs:::')
                    },
                    err => {
                        console.log('PlayRej:::', err)
                        this.isSpeaking == false
                    }
                );

            }

        }
    }

    player(requiredStringArray, currIndex) {
        // var currIndex = 0;
        console.log('curentReading:::', requiredStringArray[currIndex])
        var data: string = requiredStringArray[currIndex]
        this.speakOptions = {
            text: data,
            speakRate: 0.5,
            pitch: 1.0,
            volume: 1.0,
            locale: 'en-GB',
            queue: true,
            finishedCallback: (() => {
                if (currIndex + 1 < requiredStringArray.length) {
                    currIndex = currIndex + 1;
                    this.player(requiredStringArray, currIndex)
                }

            })
        };

        this.TTS.speak(this.speakOptions).then(
            () => {
                this.isSpeaking == true
                console.log('PlayREs:::')
            },
            err => {
                console.log('PlayRej:::', err)
                this.isSpeaking == false
            }
        );
    }
    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onReminderDialogLoaded(args: any) {
        var dialog = <any>args.object;

        setTimeout(() => {
            if (dialog.android) {
                let nativeGridMain = dialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
            } else if (dialog.ios) {
                let nativeGridMain = dialog.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 50)

    }

    onDayTap(index: number) {
        switch (index) {
            case 0:
                this.selectedDays[0] = !this.selectedDays[0];
                break;
            case 1:
                this.selectedDays[1] = !this.selectedDays[1];
                break;
            case 2:
                this.selectedDays[2] = !this.selectedDays[2];
                break;
            case 3:
                this.selectedDays[3] = !this.selectedDays[3];
                break;
            case 4:
                this.selectedDays[4] = !this.selectedDays[4];
                break;
            case 5:
                this.selectedDays[5] = !this.selectedDays[5];
                break;
            case 6:
                this.selectedDays[6] = !this.selectedDays[6];
                break;
        }
    }

    //Update to new values
    onReminderDone() {
        Values.writeString(Values.REMINDER, JSON.stringify(this.selectedDays))
        console.log("TTT::", this.selectedReminderDate.getTime())
        Values.writeString(Values.ALARM_TIME, JSON.stringify(this.selectedReminderDate.getTime()))
        if (isAndroid) {
            this.alarmUpdater.cancelAndUpdateAlarm(this.selectedReminderDate.getTime());
        }

        this.reminderDialog.hide();
    }

    //Reset to the previous values
    onReminderCancel() {
        this.reminderDialog.hide();
    }

    onValueChanged(args) {
        let progressBar = <Progress>args.object;
        console.log("Value changed for " + progressBar);
        console.log("New value: " + progressBar.value);
    }

    onPickerLoaded(args) {
        let timePicker = <TimePicker>args.object;

        timePicker.hour = 9;
        timePicker.minute = 25;
    }

    onTimeChanged(args) {
        var d = new Date(args.value);
        var c = new Date();
        this.selectedReminderDate = new Date(c.getFullYear(), c.getMonth(), c.getDate(), d.getHours(), d.getMinutes(), d.getSeconds());
        console.log("Sel:::", this.selectedReminderDate)
    }

}