import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { BookmarkComponent } from "./components/bookmark.component";


const routes: Routes = [
    { path: "", component: BookmarkComponent },
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})


export class BookmarkRoutingModule { }
