import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NgModalModule } from "../modals/ng-modal";
import { HttpClientModule } from "@angular/common/http";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { BookmarkComponent } from "./components/bookmark.component";
import { BookmarkRoutingModule } from "./bookmark-routing.module";


@NgModule({
    bootstrap: [
        BookmarkComponent
    ],
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NgModalModule,
        HttpClientModule,
        BookmarkRoutingModule
    ],
    declarations: [
        BookmarkComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})


export class BookmarkModule { }
