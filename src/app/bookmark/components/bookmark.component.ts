import { Color } from 'ui/core/view';
import { Values } from './../../values/values';
import { Component, OnInit, AfterContentInit, ViewChild } from "@angular/core";
import { UserService } from "../../services/user.service";
import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { Menu } from "nativescript-menu";
import { Router } from "@angular/router";
import { ModalComponent } from "~/app/modals/modal.component";
import { Page } from "tns-core-modules/ui/page/page";
import { ReminderService } from '~/app/services/reminder.service';
import * as LNG from "../../services/language/env.service";
import { LanguageService } from '../../services/language/env.service';


declare var android: any;
declare const CGSizeMake: any;


@Component({
    selector: "ns-bookmark",
    moduleId: module.id,
    templateUrl: "./bookmark.component.html",
    styleUrls: ['./bookmark.component.css']
})
export class BookmarkComponent implements OnInit, AfterContentInit {

    @ViewChild('reminderDialog', { static: false }) reminderDialog: ModalComponent;
    @ViewChild('languageSelectDialog', { static: false }) languageSelectDialog: ModalComponent;

    language: any;
    selectedCard: number;
    languageList: any;

    isRendering: boolean;
    renderingTimeout;

    isLoading: boolean;
    loadingTimeout;

    bookmarkHeading: string;

    bookmarks;

    constructor(private userService: UserService, private routerExtensions: RouterExtensions, private reminderService: ReminderService, private languageService: LanguageService) {

        this.languageService.languageState.subscribe((res) => {
            if (res) {
                this.language = res;

                setTimeout(() => {
                    this.routerExtensions.navigate(['']);

                }, 100);
            }
        }, error => {
            console.log("Error related to language service:", error)
        })
        this.language = this.languageService.langauge;

        this.isRendering = false;
        this.isLoading = false;

        this.selectedCard = -1;
        this.userService.activeScreen('bookmark');
        this.userService.playButtonState(false);
        this.userService.showLoadingState(false);
        this.userService.showFooter(true);

        this.bookmarkHeading = this.language.bookmarkHeading;

        this.bookmarks = [];
        this.bookmarks = JSON.parse(Values.readString('bookmarks', '[]'))
    }

    ngOnInit(): void {
        this.renderingTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 100)
    }

    ngAfterContentInit(): void {
    }

    onMenuClick(menuButton) {
        Menu.popup({
            view: menuButton,
            actions: [ { id: "two", title: this.language.setquizreminder }, { id: "three", title: this.language.changePassword }, { id: "four", title: this.language.logout }, { id: "five", title: this.language.changeLanguage }]
        })
            .then(action => {
                if (action.id == "one") {
                    alert("Clicked on Settings");


                }
                else if (action.id == "two") {
                    var body = {
                        days: [0, 6],
                        time: {
                            hours: 2,
                            minutes: 56,
                            dimension: 'am'
                        }
                    }
                    this.reminderService.showReminder();
                    // this.reminderDialog.show();
                    //hit api
                }
                else if (action.id == "three") {
                    setTimeout(() => {
                        this.isLoading = true;
                        this.routerExtensions.navigate(['/changePassword'], { clearHistory: true });
                    }, 10)
                }
                else if (action.id == "four") {
                    setTimeout(() => {
                        Values.writeString(Values.USER, "");
                        Values.writeString(Values.X_ACCESS_TOKEN, "");
                        setTimeout(() => {
                            this.isLoading = true;
                            this.routerExtensions.navigate(['/login'], { clearHistory: true });
                        }, 10)
                    }, 10)
                }
                else if (action.id == "five") {

                    setTimeout(() => {
                        this.languageSelectDialog.show();

                    }, 10)
                }
                else {

                }
            })
            .catch(console.log);
    }


    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onBookmarkLoaded(args) {
        let bookmark = args.object;

        setTimeout(() => {
            if (bookmark.android) {
                let nativeGridMain = bookmark.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
                nativeGridMain.set
            } else if (bookmark.ios) {
                let nativeGridMain = bookmark.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 20)

    }

    onBookmarkClick(bookmark: any) {
        this.selectedCard = this.bookmarks.indexOf(bookmark);
        this.userService.showLoadingState(true);
        setTimeout(() => {
            this.userService.showLoadingState(false);
            this.routerExtensions.navigate(['/chapter'], {
                queryParams: {
                    'bookmark': JSON.stringify(bookmark)
                }
            });
        }, 100);
    }

    changeLanguage(event: any) {
        console.log('changee::', event);
        this.languageSelectDialog.hide();
        this.languageService.setLanguage(event.value);
    }



    onLenguageSelectLoaded(args: any) {


        var dialog = <any>args.object;

        setTimeout(() => {
            if (dialog.android) {
                let nativeGridMain = dialog.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
            } else if (dialog.ios) {
                let nativeGridMain = dialog.ios;

                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
            // this.changeDetector.detectChanges();
        }, 50)

    }

}