import { Component, OnInit } from "@angular/core";
import { Color } from "tns-core-modules/color/color";
import { TextField } from "tns-core-modules/ui/text-field";
import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { UserService } from "~/app/services/user.service";
import { Page } from "tns-core-modules/ui/page/page";
import { Values } from "~/app/values/values";
import { HttpClient } from "@angular/common/http";
import * as LNG from "../../services/language/env.service";
import { LanguageService } from "../../services/language/env.service";

declare const android: any;
declare const CGSizeMake: any;

@Component({
    selector: "ns-forgotPassword",
    moduleId: module.id,
    templateUrl: "./forgot-password.component.html",
    styleUrls: ['./forgot-password.component.common.css', './forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
    language: any;

    isRendering: boolean;
    isLoading: boolean;
    emailText: string;
    emailHint: string;
    sendOTPButton: string;
    forgotHeading: string;

    constructor(private languageService: LanguageService, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient, private page: Page) {
        this.languageService.languageState.subscribe((res) => {
            if (res) {
                this.language = res;
            }
        }, error => {
            console.log("Error related to language service:", error)
        })
        this.language = this.languageService.langauge;

        this.isRendering = false;
        this.isLoading = false;
        this.emailText = "";
        this.emailHint = this.language.emailHint;
        this.sendOTPButton = this.language.sendOTPButton;
        this.forgotHeading = this.language.forgotHeading;
        this.userService.showFooter(false);
    }

    ngOnInit(): void {
        setTimeout(() => {
            this.isRendering = true;
        }, 300)
    }

    protected get shadowColor(): Color {
        return new Color('#888888')
    }

    protected get shadowOffset(): number {
        return 2.0
    }

    onForgotPasswordLoaded(args: any) {
        var forgotPasswordCard = <any>args.object;
        setTimeout(() => {
            if (forgotPasswordCard.android) {
                let nativeGridMain = forgotPasswordCard.android;
                var shape = new android.graphics.drawable.GradientDrawable();
                shape.setShape(android.graphics.drawable.GradientDrawable.RECTANGLE);
                shape.setColor(android.graphics.Color.parseColor('white'));
                shape.setCornerRadius(20)
                nativeGridMain.setBackgroundDrawable(shape);
                nativeGridMain.setElevation(10)
            } else if (forgotPasswordCard.ios) {
                let nativeGridMain = forgotPasswordCard.ios;
                nativeGridMain.layer.shadowColor = this.shadowColor.ios.CGColor;
                nativeGridMain.layer.shadowOffset = CGSizeMake(0, this.shadowOffset);
                nativeGridMain.layer.shadowOpacity = 0.5
                nativeGridMain.layer.shadowRadius = 5.0
                nativeGridMain.layer.shadowRadius = 5.0
            }
        }, 50)

    }

    public emailTextField(args) {
        var textField = <TextField>args.object;
        this.emailText = textField.text;
    }

    onSendOtpClick() {
        if (this.emailText == "") {
            alert("Please enter email.");
        }
        // else if (this.emailText.length < 10) {
        //     alert("Phone number should be of ten digits.");
        // }
        else {
            var forgotPasswordModel = {
                email: this.emailText
            }

            var headers = {
                'Content-Type': 'application/json'
            }

            this.http.post(Values.BASE_URL + `users/forgotPassword`, forgotPasswordModel, { headers: headers }).subscribe((res: any) => {
                if (res && res.isSuccess && res.data) {
                    console.log('Response:', res)
                    this.routerExtensions.navigate(['/confirmOtp'], {
                        queryParams: {
                            "fromForgotPassword": true,
                            "email": this.emailText
                        }
                    });
                }
            }, (error) => {
                console.log('Error:', error)
                alert(error.error.error);
                if (error.error.statusCode == 401 && error.error.error == "unauthorized_user") {
                    this.routerExtensions.navigate(['/login'], {
                        clearHistory: true
                    });
                }
                // this.errorDialog.show();
            })
        }
    }

    onBack() {
        this.routerExtensions.back();
    }

}